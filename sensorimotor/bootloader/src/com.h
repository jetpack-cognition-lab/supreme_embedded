/*---------------------------------+
 | Jetpack Cognition Lab, Inc.     |
 | Supreme Machines                |
 | Sensorimotor Bootloader         |
 | Author: Matthias Kubisch        |
 | kubisch@informatik.hu-berlin.de |
 | Last Update: January 2021       |
 +---------------------------------*/

#ifndef SUPREME_COM_H
#define SUPREME_COM_H

#include <avr/eeprom.h>
#include "uart.h"

/* wrap/unwrap the data to be sent and received into a valid
   raw data package that can be safely processed (or ignored)
   by sensorimotors. */
void send_packaged(const uint8_t* data, uint8_t size, uint8_t myid);
uint8_t recv_packaged(uint8_t* data, uint8_t myid);


/* read id from EEPROM, return default if ID was not yet set.
 * only IDs 0..127 are valid, IDs are saved with MSB set. ID | 0x80
 */
template <uint8_t default_id>
uint8_t read_id_from_EEPROM(void) {
    eeprom_busy_wait();
    uint8_t id = eeprom_read_byte((uint8_t*)0);
    /* check MSB is set to verify that the id was written before */
    return id ? (id & 0x7F) : default_id;
}

#endif /* SUPREME_COM_H */
