/*---------------------------------+
 | Supreme Machines                |
 | Sensorimotor Revision 1.3       |
 | Board Layout Configuration      |
 | Matthias Kubisch                |
 | kubisch@informatik.hu-berlin.de |
 | Last Modified: June 2021        |
 +---------------------------------*/


#ifndef SUPREME_SENSORIMOTOR_REV1_3_HPP
#define SUPREME_SENSORIMOTOR_REV1_3_HPP

/* standard libs */
#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/eeprom.h>

/* external libs*/
#include <board/pin_definitions.hpp>
#include <uart/uart.hpp>

#ifndef F_CPU
#error "F_CPU undefined, please define CPU frequency in Hz in Makefile"
#endif

#define UART_BAUD_RATE 1000000

namespace supreme {

	namespace adc_channel {
		const uint8_t position         = 3,
					  current1         = 6,
					  current2         = 7,
					  voltage_supply   = 2,
					  temperature      = 1;
	}

class sensorimotor
{
public:

	/* Arduino-like pin names */
	/* A0 */ PIN_TYPE( C, 0) read_disable; // clear/set rs485 transceiver read mode
	/* A1 */ // ADC1 Temperature
	/* A2 */ // ADC2 voltage_supply
	/* A3 */ // ADC3 Position / Potentiometer
	/* A4 */ // ADC4 I2C not in use
	/* A5 */ // ADC5 I2C not in use
	/* A6 */ // ADC6 current sensor 1
	/* A7 */ // ADC7 current sensor 2
	/* RX */ PIN_TYPE( D, 0) rxi;
	/* TX */ PIN_TYPE( D, 1) txo;
	/* D2 */ PIN_TYPE( D, 2) drive_enable ; // set/clear rs485 transceiver write mode
	/* D3 */ PIN_TYPE( D, 3) led_red      ;
	/* D4*/  PIN_TYPE( D, 4) capsense_send; // pulsed pin of capacitive sensor
	/* D5 */ PIN_TYPE( D, 5) led_yellow   ;
	/* D6 */ PIN_TYPE( D, 6) motor_disable; // set the motor's rotation direction
	/* D7 */ PIN_TYPE( D, 7) motor_n_sleep; // !sleep mode of h-bridge
	/* D8 */ // B0 not in use
	/* D9 */ PIN_TYPE( B, 1) motor_pwm_in1; // set pwm, half-bridge 1
	/* D10*/ PIN_TYPE( B, 2) motor_pwm_in2; // set pwm, half-bridge 2
	/* D11*/ // COPI not in use
	/* D12*/ // CIPO not in use
	/* D13*/ // SCK  not in use
	/* D14*/ PIN_TYPE( E, 0) capsense_recv; // reception pin of capacitive sensor
	/* D15*/ PIN_TYPE( E, 1) motor_n_fault; // hi: ok, lo: faulted.

	sensorimotor()
	{
		make_output( led_red       );
		make_output( led_yellow    );

		make_output( motor_disable );
		make_output( motor_n_sleep );
		make_output( motor_pwm_in1 );
		make_output( motor_pwm_in2 );

		make_output( txo           );
		make_output( drive_enable  );
		make_output( read_disable  );

		make_input ( motor_n_fault ); set( motor_n_fault ); // use pullup

		/* init uart rs485 bus communication at 1Mbaud/s */
		uart_init( UART_BAUD_SELECT(UART_BAUD_RATE, F_CPU) );
		rs485_recv_mode();

		sei(); // enable interrupts

		red_off();
		yellow_off();
	}


	void red_on() { set( led_red ); }
	void red_off() { clear( led_red ); }
	void red_toggle() { if( is_set(led_red)) clear(led_red); else set(led_red); }

	void yellow_on() { set( led_yellow ); }
	void yellow_off() { clear( led_yellow ); }

	void rs485_send_mode() {
		_delay_us(1); //remove_delay_ns(50);
		set(drive_enable);
		set(read_disable);
		_delay_us(10); // wait at least one bit after enabling the driver
	}

	void rs485_recv_mode() {
		_delay_us(10); // wait at least one bit after disabling the driver
		clear(drive_enable);
		clear(read_disable);
		_delay_us(1);//remove_delay_ns(70);
	}

};

/* inter integrated circuit */
/*namespace i2c {
	using SDA = A4;
	using SCL = A5;
}*/


} /* namespace supreme */

#endif /* SUPREME_SENSORIMOTOR_REV1_3_HPP */
