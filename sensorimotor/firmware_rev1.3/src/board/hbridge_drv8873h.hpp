/*---------------------------------+
 | Supreme Machines                |
 | Sensorimotor Firmware           |
 | Matthias Kubisch                |
 | kubisch@informatik.hu-berlin.de |
 | June 2021                       |
 +---------------------------------*/

#ifndef SUPREME_MOTOR_DRV8873H_HPP
#define SUPREME_MOTOR_DRV8873H_HPP

#include <util/atomic.h>
#include <board/sensorimotor_rev1_3.hpp>

extern supreme::sensorimotor board;

#define PRESCALE1 (1<<CS10)
#define PRESCALE8 (1<<CS11)
#define PRESCALE64 (1<<CS10) | (1<<CS11)

namespace supreme {

volatile uint16_t icr1 = 0, ocr1a = 0, ocr1b = 0;
volatile uint8_t tccr1b_tmp = 0;


class hbridge_drv8873h {
	/*
		Using FAST PWM mode 14:
		we need to set the TOP value

		fclock = 16 MHz
		prescaler N = 1,8,64,256,1024

		fPWM = fclock / (TOP+1)*N

		user provided pwm duty cycle: dc = 0..255
	*/
public:

	static constexpr uint8_t tccr1a = (1<<COM1A1) // Clear OC1A/OC1B on compare match, ...
	                                | (1<<COM1B1) // and set OC1A/OC1B at BOTTOM (Non-inverting mode)
	                                | (1<<WGM11); // Mode 14, Fast PWM, top = ICR1
	static constexpr uint8_t tccr1b = (1<<WGM12)
	                                | (1<<WGM13); // prescaler is set separately later
private:

	// default freqency 16.000 kHz (clock) / 256 == 62.5 kHz
	uint16_t val = 256;
	uint16_t A = (val >> 3); // 32
	uint16_t period_0 = 0;
	uint8_t direction = 0;
	bool period_changed = true;

public:

	hbridge_drv8873h()
	{
		/* setup motor bridge */
		set  ( board.motor_disable ); // motor bridge disabled by default
		clear( board.motor_pwm_in1 ); // disable pwm pin 1
		clear( board.motor_pwm_in2 ); // disable pwm pin 2
		set  ( board.motor_n_sleep ); // enable motor bridge logic

		/* configure timer 1 for PWM mode */
		TCCR1A = tccr1a;
		TCCR1B = tccr1b;
		icr1 = val - 1;
		ICR1 = icr1; /* The 16bit ICR1 register is NOT double buffered,
		                so start counter after initializing */
		OCR1A = 0;
		OCR1B = 0;
		tccr1b_tmp = tccr1b | PRESCALE1;
		TCCR1B = tccr1b_tmp;

	}

	void set_dir(bool dir) { direction = dir ? 1u : 0u; }

	/*  PWM Mode Truth Table
	    nSLEEP DISABLE EN/IN1 PH/IN2 OUT1 OUT2
	    0      X       X      X      Hi-Z Hi-Z
	    1      1       X      X      Hi-Z Hi-Z
	    1      0       0      0      Hi-Z Hi-Z  COASTING
	    1      0       0      1      L    H     REVERSE
	    1      0       1      0      H    L     FORWARD
	    1      0       1      1      H    H     BRAKE

	    ++====+------+        ++====+------+        ++==
	    || br |  dc  |  rem.  || br |  dc  |  rem.  ||
	    ||    |      |        ||    |      |        ||
	  --++    +------+========++    +------+========++
	    \_____________________/
	        full pwm cycle

	  F +----------------------+   + if only using braking (high friction)
	    |                +   * |   - if only using coasting (low force)
	    |           +     *    |   * using braking + coasting (optimal)
	    |      +       *       |
	    | +         *        - |
	    |        *       -     |
	    |     *     -          |
	    |  *   -               |
	    +----------------------+
	    0%    duty cycle    100%
	*/


	void set_pwm(uint8_t dc) {
		if (dc != 0)
		{
			/* use 1/4th braking of the remaining (non-driving) time */
			const uint8_t br = (255 - dc) >> 2;
			dc += br;

			const uint16_t drive = ((uint32_t) A * dc) >> 5; // val is in 1/32, so we need to shift 5
			const uint16_t brake = ((uint32_t) A * br) >> 5;

			if (direction) {
				ocr1a = drive;
				ocr1b = brake;
			} else {
				ocr1a = brake;
				ocr1b = drive;
			}
		}
		else {
			ocr1a = OCR1A >> 1; // set pwm towards zero duty cycle
			ocr1b = OCR1B >> 1;
		}

		if (period_changed) {
			period_changed = false;
			TIMSK1 |= (1<<TOIE1); // enable interrupt on timer 1 overflow
		} else {
			OCR1A = ocr1a; // update without changing icr1
			OCR1B = ocr1b;
		}
	}

	void disable(void) { set( board.motor_disable ); }
	void enable(void) { clear( board.motor_disable ); }

	bool is_faulted(void) const { return !is_set( board.motor_n_fault ); }

	/*  variable frequency PWM mode:

		32.7032 Hz Note C1, T = 30.578 ms = 30578 us --> TOP=61156 (2*T, N=8, base = 2,000,000)
		34.6478 Hz Note D1, T = 28.862 ms = 28862 us --> TOP=57724 (2*T, N=8, base = 2,000,000)
		...
		4186.01 Hz Note C8, T = 0.238891 s ~= 239 us --> TOP=3824  (16*T, N=1, base = 16,000,000)

		set the period duration in us (fits in 2 Bytes)
		value range period_us 10...50,000
		frequency range 10kHz...20Hz
	*/

	void set_pwm_period(uint16_t period_us) {
		if (period_us == period_0) return;
		period_0 = period_us;

		if (period_us < 4096) {          // 100 kHz -- 244.2 Hz
			if (period_us < 10) period_us = 10; // upper limit 100 kHz
			tccr1b_tmp = tccr1b | PRESCALE1;
			val = (period_us << 4); // 1*16
		}
		else if (period_us < 32768) {    // 244.14 Hz -- 30.5185 Hz
			tccr1b_tmp = tccr1b | PRESCALE8;
			val = (period_us << 1); // 8*2
		}
		else {                          // 30.5175 Hz -- 20 Hz
			if (period_us > 50000) period_us = 50000; // lower limit 20 Hz
			tccr1b_tmp = tccr1b | PRESCALE64;
			val = (period_us >> 2); // 64*(1/4)
		}

		/* set amplification factor */
		A = (val >> 3); // div by 8 to get 32th
		ATOMIC_BLOCK(ATOMIC_FORCEON) {
			icr1 = val - 1;
		}
		period_changed = true;
		/* Note: after this we need to call
		   set_pwm() to enable the updated ICR1 value */
	}
};

ISR(TIMER1_OVF_vect) {
	TIMSK1 &= ~(1<<TOIE1); // disable interrupt on timer 1 overflow
	TCCR1B = hbridge_drv8873h::tccr1b; // disable timer
	ICR1 = icr1; // update TOP for timer 1
	OCR1A = ocr1a;
	OCR1B = ocr1b;
	TCCR1B = tccr1b_tmp; // start with new clock prescaler
}

} /* namespace supreme */


#endif /* SUPREME_MOTOR_DRV8873H_HPP */
