/*---------------------------------+
 | Supreme Machines                |
 | Sensorimotor Firmware           |
 | Matthias Kubisch                |
 | kubisch@informatik.hu-berlin.de |
 | March 2021                      |
 +---------------------------------*/

#ifndef SUPREME_RANDOM_HPP
#define SUPREME_RANDOM_HPP

namespace supreme {

class random
{
	uint16_t lfsr = 0;

	uint16_t galois_lfsr16_next(uint16_t n) { return (n >> 0x01U) ^ (-(n & 0x01U) & 0xB400U); }

public:

	random(uint16_t seed) : lfsr(seed) {}

	uint16_t step(void) {
		lfsr = galois_lfsr16_next(lfsr);
		return lfsr;
	}
};

}

#endif /* SUPREME_RANDOM_HPP */
