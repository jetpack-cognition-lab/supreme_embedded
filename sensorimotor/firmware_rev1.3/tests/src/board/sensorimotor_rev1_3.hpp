#ifndef TEST_SENSORIMOTOR_REV1_3_HPP
#define TEST_SENSORIMOTOR_REV1_3_HPP

#include <uart/uart.hpp>

typedef unsigned char  uint8_t;
typedef unsigned short uint16_t;

unsigned TCCR0A = 0, TCCR0B = 0, OCR0B  = 0;
const unsigned WGM00 = 0, WGM01 = 0, CS00 = 0, COM0B1 = 0;


namespace supreme {

class sensorimotor {
public:

	void red_off() {}
	void red_on() {}
	void yellow_on() {}
	void yellow_off() {}

	void rs485_send_mode() { ++stats.send_enable; }
	void rs485_recv_mode() { ++stats.recv_enable; }

	struct {
		unsigned send_enable  = 0;
		unsigned recv_enable  = 0;

		void clear() {
			send_enable  = 0;
			recv_enable  = 0;
		}
	} stats;
	// UART
//	bool transmission_complete(void) { return true; }
};

sensorimotor board;
}

inline void _delay_ms (unsigned /*d*/) {}


void reset_hardware() {
	_uart.out_buffer.clear();
	_uart.transmit_done = false;
	_uart.in_queue.clear();
	supreme::board.stats.clear();
}

#endif /* TEST_SENSORIMOTOR_REV1_3_HPP */
