/*---------------------------------+
 | Jetpack Cognition Lab, Inc.     |
 | Supreme Machines                |
 | Sensorimotor Rev 1.2 Firmware   |
 | Matthias Kubisch                |
 | kubisch@informatik.hu-berlin.de |
 | January 2021                    |
 +---------------------------------*/

#include "./board/sensorimotor_rev1_2.hpp"
#include "./board/hbridge_ifx9201sg.hpp"
#include "./system/core.hpp"
#include "./system/communication.hpp"
#include "./system/adc.hpp"
#include "./system/bootloader.hpp"
#include "./system/fuses.hpp"
#include "./system/read_vcc.hpp"

supreme::sensorimotor board; /* initialize sensorimotor board */

/* Disable all previous watchdog timer activities of the
   bootloader, in order to not interfere with the firmware.*/
void wdt_init(void) __attribute__((naked)) __attribute__((section(".init3")));

void wdt_init(void) {
	MCUSR &= ~(_BV(WDRF)); //TODO comment
	wdt_disable();
}


/* this is called once TCNT2 = OCR2A = 249 *
 * resulting in a 1 ms cycle time, 1kHz    */
volatile bool current_state = false;
ISR (TIMER2_COMPA_vect)
{
	current_state = !current_state;
}


int main()
{
	using namespace supreme;

	/* check for stable supply voltage before starting */
	supreme::adc::enable();
	while(supreme::read_supply_voltage_mV() < 3135) { // 95% of 3.300 mV
		board.red_toggle(); _delay_ms(64);
	}

	cli();

	/* clock is set to be div8 (2MHz) by default, to ensure safe power up.
	   however, when safe voltage is reached, set clock to 16MHz */
	CLKPR = (1 << CLKPCE); // Enable change of CLKPS bits
	CLKPR = 0; // Set prescaler to 1

	sei();


	/* jump to bootloader on reset-pin pulled low */
	if (bit_is_set(MCUSR, EXTRF)) {
		MCUSR &= ~(_BV(EXTRF));
		start_bootloader();
	}

	if (not check_fuses(constants::LF, constants::HF, constants::EF))
		stop(constants::assertion::wrong_fuse_bits_set);

	supreme::adc::init();
	supreme::adc::restart();

	typedef sensorimotor_core<supreme::hbridge_ifx9201sg> core_t;
	core_t core;


	/* Design of the 1kHz main loop:
	 * 16Mhz clock, prescaler 64 -> 16.000.000 / 64 = 250.000 increments per second
	 * diveded by 1000 -> 250 increments per ms
	 * hence, timer compare register to 250-1 -> ISR inc ms counter -> 1kHz loop
	 *
	 * configure timer 2:
	 */
	TCCR2A = (1<<WGM21);             // CTC mode
	TCCR2B = (1<<CS22);              // set prescaler to 64
	OCR2A = 249;                     // set timer compare register to 250-1
	TIMSK2 = (1<<OCIE2A);            // enable compare interrupt

	communication_ctrl<core_t>/*, exts_t>*/ com(core);//, exts);
	bool previous_state = false;


	sei(); // enable interrupts

	core.init_sensors();
	while(1) /* main loop */
	{
		if (com.step()) {
			core.halt();
			start_bootloader();
		}
		if (current_state != previous_state) {
			board.red_on();     // red led on, begin of cycle
			core.step();
			supreme::adc::restart();
			board.red_off();    // red led off, end of cycle
			previous_state = current_state;
		}
	}
	return 0;
}
