/*---------------------------------+
 | Supreme Machines                |
 | Sensorimotor Revision 1.2       |
 | Board Layout Configuration      |
 | Matthias Kubisch                |
 | kubisch@informatik.hu-berlin.de |
 | Last Modified: December 2020    |
 +---------------------------------*/


#ifndef SUPREME_SENSORIMOTOR_REV1_2_HPP
#define SUPREME_SENSORIMOTOR_REV1_2_HPP

/* standard libs */
#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/eeprom.h>

/* external libs*/
#include "pin_definitions.hpp"
#include "../uart/uart.hpp"

#ifndef F_CPU
#error "F_CPU undefined, please define CPU frequency in Hz in Makefile"
#endif

#define UART_BAUD_RATE 1000000

namespace supreme {

	namespace adc_channel {
		const uint8_t position         = 2,
		              current          = 6,
		              voltage_supply   = 7,
		              temperature      = 1;
	}

class sensorimotor
{
public:

    /* Arduino-like pin names */
    /* A0 */ PIN_TYPE( C, 0) read_disable; // clear/set rs485 transceiver read mode
    /* A1 */ // ADC1 Temperature
    /* A2 */ // ADC2 Position / Potentiometer
    /* A3 */ // ADC3 not in use
    /* A4 */ // ADC4 not in use
    /* A5 */ // ADC5 not in use
    /* A6 */ // ADC6 current sensor
    /* A7 */ // ADC7 voltage_supply
    /* RX */ PIN_TYPE( D, 0) rxi;
    /* TX */ PIN_TYPE( D, 1) txo;
    /* D2 */ PIN_TYPE( D, 2) drive_enable; // set/clear rs485 transceiver write mode
    /* D3 */ PIN_TYPE( D, 3) led_red     ;
    /* D5 */ PIN_TYPE( D, 5) led_yellow  ;
    /* D6 */ PIN_TYPE( D, 6) motor_dir   ; // set the motor's rotation direction
    /* D7 */ PIN_TYPE( D, 7) motor_vso   ; // enable power supply of h-bridge logic
    /* D8 */ PIN_TYPE( B, 0) motor_dis   ; // disable h-bridge bride
    /* D9 */ PIN_TYPE( B, 1) motor_pwm   ; // set pulse width modulated motor voltage

    sensorimotor()
    {
        make_output( led_red      );
        make_output( led_yellow   );

        make_output( motor_dir    );
        make_output( motor_vso    );
        make_output( motor_dis    );
        make_output( motor_pwm    );

        make_output( txo          );
        make_output( drive_enable );
        make_output( read_disable );

		//clear(read_disable);

     	/* init uart rs485 bus communication at 1Mbaud/s */
        uart_init( UART_BAUD_SELECT(UART_BAUD_RATE, F_CPU) );
        rs485_recv_mode();

        sei(); // enable interrupts

        red_off();
        yellow_off();
    }


    void red_on() { set( led_red ); }
    void red_off() { clear( led_red ); }
	void red_toggle() { if( is_set(led_red)) clear(led_red); else set(led_red); }

    void yellow_on() { set( led_yellow ); }
    void yellow_off() { clear( led_yellow ); }

    void rs485_send_mode() {
       _delay_us(1); //remove_delay_ns(50);
        set(drive_enable);
        set(read_disable);
        _delay_us(10); // wait at least one bit after enabling the driver
    }

    void rs485_recv_mode() {
        _delay_us(10); // wait at least one bit after disabling the driver
        clear(drive_enable);
        clear(read_disable);
        _delay_us(1);//remove_delay_ns(70);
    }



    /* TODO: complete this list
    using A0 = GpioC0;
    using A1 = GpioC1;
    using A2 = GpioC2;
    using A3 = GpioC3;
    using A4 = GpioC4;
    using A5 = GpioC5;
    using A6 = GpioE2;
    using A7 = GpioE3;

    using D0  = GpioD0;
    using D1  = GpioD1;
    using D2  = GpioD2;
    using D3  = GpioD3;
    using D4  = GpioD4;
    using D5  = GpioD5;
    using D6  = GpioD6;
    using D7  = GpioD7;
    using D8  = GpioB0;
    using D9  = GpioB1;
    using D10 = GpioB2;
    using D11 = GpioB3;
    using D12 = GpioB4;
    using D13 = GpioB5;
*/
};






/* serial peripheral interface */
/*namespace spi {
	using CSN = D10;
	using SDI = D11;
	using SDO = D12;
	using SCK = D13;
}*/

/* inter integrated circuit */
/*namespace i2c {
	using SDA = A4;
	using SCL = A5;
}*/


} /* namespace supreme */

#endif /* SUPREME_SENSORIMOTOR_REV1_2_HPP */
