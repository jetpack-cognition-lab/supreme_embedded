/*---------------------------------+
 | Jetpack Cognition Lab, Inc.     |
 | Supreme Machines                |
 | Sensorimotor Firmware           |
 | Matthias Kubisch                |
 | kubisch@informatik.hu-berlin.de |
 | Juli 6th 2021                   |
 +---------------------------------*/

#ifndef SUPREME_TEMP_MCP9701A_HPP
#define SUPREME_TEMP_MCP9701A_HPP

/*----------------------------------------------------------------------------+
 |  TEMPSENSOR MCP9701A                                                       |
 |                                                                            |
 |  From the datasheet:                                                       |
 |  "The MCP9701A [...] measures temperature by monitoring the voltage of a   |
 |   diode located in the die. A low-impedance thermal path between the die   |
 |   and the PCB is provided by the pins. Therefore, the sensor effectively   |
 |   monitors the temperature of the PCB. However, the thermal path for the   |
 |   ambient air is not as efficient because the plastic device package       |
 |   functions as a thermal insulator from the die."                          |
 |                                                                            |
 |  approx. linear range: 0 -- +70 C (degree Celsius)                         |
 |  extended range -20 -- +150 C                                              |
 |  +/-2 deg accuracy in linear range                                         |
 |  output non-linearity +/- 0.5 C                                            |
 |  Ta: Ambient Temperature                                                   |
 |                                                                            |
 |  Voltage at 0C = V0 = 400mV                                                |
 |  0 -- 3V3 -> 0...1023 (10bit)                                              |
 |  a = 1023/3V3 = 1023/3300mV = 0.31 1/mV                                    |
 |  u0 = V * a = 400mV * 0.31 / mV = 124                                      |
 |                                                                            |
 |  Temperature Coefficient = Tc = 19.5mV/C                                   |
 |  tc = 19.5mv/C * 0.31 1/mV = 6.045 1/C                                     |
 |                                                                            |
 |  Example:                                                                  |
 |  Vout = Tc * Ta + V0                                                       |
 |       = 19.5mV/C * 20C + 400mV = 790mV                                     |
 |                                                                            |
 |  Vmin = 0mV = Tc * Ta + V0                                                 |
 |  -V0/Tc = -400mV C / 19.5mV  ~= -20.5C                                     |
 |                                                                            |
 |  Target format is int16_t values in 10mC = 1cC,                            |
 |  so we need a factor of 100.                                               |
 |                                                                            |
 |  val/100 = (v_adc - u0) / tc                                               |
 |          = (v_adc - 124) / 6.045                                           |
 |                                                                            |
 |      val = 100 * (v_adc - 124) / 6.045                                     |
 |          = 16,5 * (v_adc - 124)                                            |
 +----------------------------------------------------------------------------*/


namespace supreme {

inline int16_t get_temperature_celsius(uint16_t v_adc)
{
	/* return value in 0.01 degree Celsius */
	const int16_t v0 = (v_adc - 124); // voltage with zero offset
	return 16 * v0 + (v0 >> 1); // 16,5 * v0
}

} /* namespace supreme */

#endif /* SUPREME_TEMP_MCP9701A_HPP */
