/*---------------------------------+
 | Supreme Machines                |
 | Sensorimotor Firmware           |
 | Matthias Kubisch                |
 | kubisch@informatik.hu-berlin.de |
 | December 2020                   |
 +---------------------------------*/

#ifndef SUPREME_MOTOR_IFX9201SG_HPP
#define SUPREME_MOTOR_IFX9201SG_HPP

#include "sensorimotor_rev1_2.hpp"

extern supreme::sensorimotor board;

#define PRESCALE1 (1<<CS10)
#define PRESCALE8 (1<<CS11)
#define PRESCALE64 (1<<CS10) | (1<<CS11)

namespace supreme {

class hbridge_ifx9201sg {
	/*
		Using FAST PWM mode 14:
		we need to set the TOP value

		fclock = 16 MHz
		prescaler N = 1,8,64,256,1024

		fPWM = fclock / (TOP+1)*N

		user provided pwm duty cycle: dc = 0..255
	*/

	static constexpr uint8_t tccr1a = (1<<COM1A1)
	                                | (1<<WGM11); // Mode 14, Fast PWM, top = ICR1
	static constexpr uint8_t tccr1b = (1<<WGM12)
	                                | (1<<WGM13); // prescaler set separately

	// default: Voicemode=OFF
	// 16.000 kHz (clock) / 800 == 20 kHz
	uint16_t val = 800;
	uint16_t A = (val >> 3);
	uint16_t period_0 = 0;

public:

	hbridge_ifx9201sg()
	{
		/* setup motor bridge */
		set  ( board.motor_dis ); // motor bridge disabled by default
		clear( board.motor_pwm ); // disable pwm pin
		set  ( board.motor_vso ); // enable motor bridge logic
		set  ( board.motor_dir ); // set direction

		/* configure timer 1 for PWM mode */
		TCCR1A = tccr1a;
		TCCR1B = tccr1b | PRESCALE1;
		ICR1 = val-1;
		OCR1A = 0;
	}

	void set_dir(bool dir) {
		if (dir) set( board.motor_dir );
		else clear( board.motor_dir );
	}

	void set_pwm(uint8_t dc) {
		if (dc != 0) {
			clear( board.motor_dis );
			OCR1A = ((uint32_t) A * dc) >> 5; // val is in 1/32, so we need to shift 5
		}
		else {
			set( board.motor_dis );
			OCR1A = 0; // set pwm to zero duty cycle
		}
	}

	/*  Voicemode:
		40 Hz -- 4.000 Hz my be sufficient, and OFF: 20kHz

		32.7032 Hz Note C1, T = 30.578 ms = 30578 us --> TOP=61156 (2*T, N=8, base = 2,000,000)
		34.6478 Hz Note D1, T = 28.862 ms = 28862 us --> TOP=57724 (2*T, N=8, base = 2,000,000)
		...
		4186.01 Hz Note C8, T = 0.238891 s ~= 239 us --> TOP=3824  (16*T, N=1, base = 16,000,000)

		set the period duration in us (fits in 2 Bytes)
		value range 50...50,000
		freq range 20...20,000 Hz
	*/

	void set_pwm_period(uint16_t period_us) {
		if (period_us == period_0) return;
		period_0 = period_us;

		uint8_t tccr1b_tmp = 0;

		if (period_us < 4096) {
			if (period_us < 50) period_us = 50; // upper limit 20 kHz
			tccr1b_tmp = tccr1b | PRESCALE1;
			val = (period_us << 4); // 1*16
		}
		else if (period_us < 32768) {
			tccr1b_tmp = tccr1b | PRESCALE8;
			val = (period_us << 1); // 8*2
		} else {
			if (period_us > 50000) period_us = 50000; // lower limit 20 Hz
			tccr1b_tmp = tccr1b | PRESCALE64;
			val = (period_us >> 2); // 64*(1/4)
		}

		/* set amplification factor */
		A = (val >> 3); // div by 8 to get 32th
		TCCR1B = 0;
		OCR1A = 0;
		TCNT1 = 0;
		ICR1 = val - 1; // set TOP
		TCCR1B = tccr1b_tmp;
		/* Note: after this you need to update the OCR1A */
	}
};

//using motordriver_t = motor_ifx9201sg;

} /* namespace supreme */


#endif /* SUPREME_MOTOR_IFX9201SG_HPP */
