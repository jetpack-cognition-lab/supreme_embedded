/*---------------------------------+
 | Supreme Machines                |
 | Sensorimotor Firmware           |
 | Matthias Kubisch                |
 | kubisch@informatik.hu-berlin.de |
 | December 2020                   |
 +---------------------------------*/

#ifndef SUPREME_MOTOR_IFX9201SG_HPP
#define SUPREME_MOTOR_IFX9201SG_HPP

#include "sensorimotor_rev1_2.hpp"
#include "../common/random.hpp"

extern supreme::sensorimotor board;

volatile uint8_t rndval = 0;
supreme::random rnd(0x4223);
volatile uint8_t dc0 = 0;

ISR (TIMER1_COMPA_vect)
{
	rndval = rnd.step();
	if (dc0 > rndval) set( board.motor_pwm );
	else clear( board.motor_pwm );
}

namespace supreme {

class hbridge_ifx9201sg_rnd {



	static constexpr uint8_t tccr1a = 0;//(1<<WGM11); // Mode 14, Fast PWM, top = ICR1
	static constexpr uint8_t tccr1b = (1<<WGM12)
	                                //| (1<<WGM13)
	                                | (1<<CS11 ); // prescaler 8

	// 16,000,000 MHz (clock) / 8 == 2,000,000 increments per second
	// diveded by 1000 -> 2000 increments per ms
	// hence, timer compare register to 200-1 -> ISR inc ms counter -> 10kHz loop


public:

	hbridge_ifx9201sg_rnd()
	{
		/* setup motor bridge */
		set  ( board.motor_dis ); // motor bridge disabled by default
		clear( board.motor_pwm ); // disable pwm pin
		set  ( board.motor_vso ); // enable motor bridge logic
		set  ( board.motor_dir ); // set direction

		/* configure timer 1 for PWM mode */
		TCCR1A = tccr1a;
		TCCR1B = tccr1b;
		OCR1A = 500; // set timer compare register
		TIMSK1 = (1<<OCIE1A); // enable compare interrupt
	}

	void set_dir(bool dir) {
		if (dir) set( board.motor_dir );
		else clear( board.motor_dir );
	}

	void set_pwm(uint8_t dc) {
		if (dc != 0) {
			clear( board.motor_dis );
			dc0 = dc;
		}
		else {
			set( board.motor_dis );
			dc0 = 0; // set pwm to zero duty cycle
		}
	}

	void set_pwm_period(uint16_t /*period_us*/) {
		/*
		if (period_us == period_0) return;
		period_0 = period_us;

		uint8_t tccr1b_tmp = 0;

		if (period_us < 4096) {
			if (period_us < 50) period_us = 50; // upper limit 20 kHz
			tccr1b_tmp = tccr1b | PRESCALE1;
			val = (period_us << 4); // 1*16
		}
		else if (period_us < 32768) {
			tccr1b_tmp = tccr1b | PRESCALE8;
			val = (period_us << 1); // 8*2
		} else {
			if (period_us > 50000) period_us = 50000; // lower limit 20 Hz
			tccr1b_tmp = tccr1b | PRESCALE64;
			val = (period_us >> 2); // 64*(1/4)
		}
		*/

		/* set amplification factor */
		/*
		A = (val >> 3); // div by 8 to get 32th
		TCCR1B = 0;
		OCR1A = 0;
		TCNT1 = 0;
		ICR1 = val - 1; // set TOP
		TCCR1B = tccr1b_tmp;
		*/
		/* Note: after this you need to update the OCR1A */
	}
};

//using motordriver_t = motor_ifx9201sg;

} /* namespace supreme */


#endif /* SUPREME_MOTOR_IFX9201SG_HPP */
