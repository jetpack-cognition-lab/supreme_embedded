/*---------------------------------+
 | Jetpack Cognition Lab, Inc.     |
 | Supreme Machines                |
 | Sensorimotor Rev 1.2 Firmware   |
 | Matthias Kubisch                |
 | kubisch@informatik.hu-berlin.de |
 | January 2021                    |
 +---------------------------------*/

#ifndef SUPREME_COMMON_ASSERT_HPP
#define SUPREME_COMMON_ASSERT_HPP

#include "../board/sensorimotor_rev1_2.hpp"
#include "constants.hpp"

extern supreme::sensorimotor board; // find better way

namespace supreme {

void blink(uint8_t code) {
	for (uint8_t i = 0; i < 8; ++i)
	{
		if ((code & (0x1 << i)) == 0)
		{
			board.red_off();
			board.yellow_on();
		} else {
			board.red_on();
			board.yellow_off();
		}
		_delay_ms(250);
		board.red_off();
		board.yellow_off();
		_delay_ms(250);
	}
}

void assert(bool condition, constants::assertion code) {
	if (condition) return;
	//TODO halt PWMs and other stuff

	board.red_off();
	board.yellow_off();
	while(1) {
		blink(static_cast<uint8_t>(code));
		_delay_ms(1000);
	}
}

inline void stop(constants::assertion code) { assert(false, code); }

} /* namespace supreme */

#endif /* SUPREME_COMMON_ASSERT_HPP */
