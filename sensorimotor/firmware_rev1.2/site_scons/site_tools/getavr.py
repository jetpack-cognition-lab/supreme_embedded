"""
 +----------------------------------+
 | Jetpack Cognition Lab, Inc.      |
 | Supreme Sensorimotor rev 1.2     |
 | Firmware Build Script            |
 | Matthias Kubisch                 |
 | kubisch@informatik.hu-berlin.de  |
 | December 2020                    |
 +----------------------------------+

"""

# LICENSE: GPL 3.0
# Contribution: This script was based on ideas of the AVR/Scons example by Marin Ivanov
# (https://github.com/metala/avr-gcc-scons-skel)


import os, re, subprocess
import SCons.Util
import SCons.Tool.cxx as cxx

compiler = 'avr-g++'
objcopy  = 'avr-objcopy'


def generate(env):
    """Add Builders and construction variables for gcc to an Environment."""
    cxx.generate(env)

    env['CXX'        ] = env.Detect(compiler)
    env['OBJCOPY'    ] = env.Detect(objcopy)
    env['SHCCFLAGS'  ] = SCons.Util.CLVar('$CCFLAGS')

    env.Append( BUILDERS = { 'Elf': _get_elf_builder()
                           , 'Hex': _get_hex_builder()
                           , 'Bin': _get_bin_builder()
                           })


def exists(env):
    return env.Detect(compiler) and env.Detect(objcopy)

def _get_elf_builder():
    return SCons.Builder.Builder(action = "$CXX -mmcu=${MCU} -Wl,-Map=${TARGET}.map -Os -Xlinker -Map=${TARGET}.map -Wl,--gc-sections -o ${TARGET} ${SOURCES}")

def _get_hex_builder():
    return SCons.Builder.Builder(action = "$OBJCOPY -O ihex -R .eeprom $SOURCES $TARGET")

def _get_bin_builder():
    return SCons.Builder.Builder(action = "$OBJCOPY -R .fuse -R .eeprom -R .lock -R .signature -S -O binary $SOURCES $TARGET")

