/*---------------------------------+
 | Jetpack Cognition Lab, Inc.     |
 | Supreme Machines Sensorimotor   |
 | Firmware for Board-Revision 1.3 |
 | Author: Matthias Kubisch        |
 | kubisch@informatik.hu-berlin.de |
 | June 2021                       |
 +---------------------------------*/

#ifndef SUPREME_READ_VCC_HPP
#define SUPREME_READ_VCC_HPP

#include <avr/io.h>
#include <util/delay.h>

namespace supreme {

/**
 * Reads supply voltage of Atmega328p(b) internally without any external circuitry
 *
 * REFS1 | REFS0 | Voltage Reference Selection
 *     0 |     0 | AREF, Internal Vref turned OFF
 *     0 |     1 | AVcc with external capacitor at AREF pin
 *     1 |     0 | Reserved
 *     1 |     1 | Internal 1.1V voltage reference with ext. cap at AREF pin
 *
 * MUX3...0 Single Ended Input
 * 0000 ADC0
 * 0001 ADC1
 * 0010 ADC2
 * 0011 ADC3
 * 0100 ADC4
 * 0101 ADC5
 * 0110 ADC6
 * 0111 ADC7
 * 1000 ADC8 (1)
 * 1001 (reserved)
 * 1010 (reserved)
 * 1011 (reserved)
 * 1100 (reserved)
 * 1101 (reserved)
 * 1110 1.1V (VBG)
 * 1111 0V (GND)
 */

uint16_t read_supply_voltage_mV()
{
	/* Read 1V1 reference (Vbg) against AVcc
	 * set the reference to Vcc and the measurement to the internal 1V1 reference */
	ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
	_delay_us(500); /* wait for Vref to settle */

	ADCSRA |= _BV(ADSC);             /* start conversion */
	while (bit_is_set(ADCSRA,ADSC)); /* measure */

	return (1100UL*1023/ADC);        /* AVcc = Vbg / ADC * 1023
	                                         = 1.1V * 1023 / ADC */
}

} /* namespace supreme */

#endif /* SUPREME_READ_VCC_HPP */
