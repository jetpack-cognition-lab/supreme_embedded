/*---------------------------------+
 | Jetpack Cognition Lab, Inc.     |
 | Supreme Machines                |
 | Energymodule Firmware           |
 | Matthias Kubisch                |
 | kubisch@informatik.hu-berlin.de |
 | May 2021                        |
 +---------------------------------*/

#ifndef SUPREME_ENERGYMODULE_CORE_HPP
#define SUPREME_ENERGYMODULE_CORE_HPP

#include "../board/energymodule.hpp"
#include "../system/adc.hpp"
#include "../system/assert.hpp"
#include "../system/ledpwm.hpp"

extern supreme::energymodule board;

namespace supreme {

enum StatusBits : uint8_t
{
	button_pressed      =  0,
	charger_connected   =  1,
	battery_charging    =  2,
	limiter_fault       =  3,
	vbus_charging       =  4,
	no_battery_inserted =  5,
//	reserved_6          =  6,
	shutdown_initiated  =  7,
	vbat_ov_warning     =  8,
	vbat_uv_warning     =  9,
	vbat_uv_error       = 10,
	vbus_ov_warning     = 11,
	vbus_uv_warning     = 12,
//	reserved_13         = 13,
//	reserved_14         = 14,
//	reserved_15         = 15,
};

enum ModuleState : uint8_t
{
	initializing   = 0,
	powered_off       ,
	shutting_down     ,
	notify_subsystems ,
	standby_vbus_low  ,
	active_vbus_high  ,
};

namespace defaults {
	const uint16_t shutdown_timeout_ms = 10000; // 10 s
	const uint16_t no_data_timeout_ms = 100; // 100 ms

	/* button times */
	const uint16_t power_off_time_ms = 1500; // 1.5 s
	const uint16_t power_on_time_ms = 250; // 250 ms

	const uint16_t initial_wait_time_ms = 2000;

	/*---------------------------------+
	 |             56    1023          |
	 |   Vbat0 = ----- x ---- x Vbat   |
	 |           56+18   3V3           |
	 +---------------------------------*/
	const uint16_t vbat_ov_threshold_warning = 986; // ~ 4V2
	const uint16_t vbat_uv_threshold_warning = 750; // ~ 3V2
	const uint16_t vbat_uv_threshold_error   = 680; // ~ 2V9

	/*---------------------------------+
	 |             10    1023          |
	 |   Vbus0 = ----- x ---- x Vbus   |
	 |           10+10   3V3           |
	 +---------------------------------*/
	const uint16_t vbus_ov_threshold_warning = 930; // ~ 6V
	const uint16_t vbus_uv_threshold_warning = 620; // ~ 4V

	const uint16_t vbus_active_voltage       = 925; // ~ 5V96
	const uint16_t vbus_reg_voltage_base     = vbus_active_voltage-255;
	const uint16_t vbus_standby_voltage      = 775; // ~ 5V
	const uint16_t vbus_shutdown_voltage     = 620; // ~ 4V

	const uint8_t deadband = 2; //TODO: experiment, to reduce clicker sound
}

class Sensors {
public:

	float voltage_bat = .0f;
	float voltage_bus = .0f;

	bool button_pressed    = false;
	bool limiter_faulted   = false;
	bool charger_connected = false;
	bool battery_charging  = false;

	void step(void)
	{
		/* buffer the inputs state for this cycle */

		voltage_bat       = .9f*voltage_bat + .1f*adc::result[adc::voltage_bat]; // simple IIR low-pass filter
		voltage_bus       = .9f*voltage_bus + .1f*adc::result[adc::voltage_bus];

		button_pressed    = board.is_button_pressed();
		limiter_faulted   = board.is_limiter_faulted();
		charger_connected = board.is_charger_connected();
		battery_charging  = board.is_battery_charging();
	}
};


class energymodule_core {

	ModuleState      module_state = initializing;
	Sensors          sensors;
	Pulsed_LED       yellow;

	uint16_t         no_data_time_ms = 0;
	uint16_t         initial_time_ms = 0;
	uint16_t          notify_time_ms = 0;

	uint16_t         status_msg = 0;
	uint16_t         bpressed_counter = 0;
	uint8_t          target_level = 0;
	bool             user_initiated_shutdown = false;

public:

	energymodule_core()
	: sensors()
	, yellow()
	{
		yellow.enable();
	}

	bool is_shutdown_condition(void) {
		return user_initiated_shutdown    // user pressed power button
		    or !is_battery_power_on()     // user enforced hard power off, or battery removed
		    or sensors.limiter_faulted    // limiter overloaded
		// TODO what else?
		;
	}

	bool is_standby_condition(void) {
		return timeout_no_data_received()
		    or sensors.battery_charging
		    or sensors.charger_connected
		// TODO what else?
		;
	}

	void apply_module_state(void)
	{
		switch(module_state)
		{
		case active_vbus_high:
			yellow.set_pwm(64);
			active_bus_regulation();
			if (no_data_time_ms < defaults::no_data_timeout_ms) no_data_time_ms++;
			if (is_standby_condition ()) module_state = standby_vbus_low;
			if (is_shutdown_condition()) module_state = notify_subsystems;
			break;

		case standby_vbus_low:
			yellow.idle();
			regulated_vbus(defaults::vbus_standby_voltage);
			no_data_time_ms = 0;
			if (is_shutdown_condition()) {
				module_state = notify_subsystems;
			}
			break;

		case notify_subsystems:
			yellow.pulse<2,24>();
			++notify_time_ms;
			regulated_vbus(defaults::vbus_shutdown_voltage);
			if (timeout_shutdown_clients() or !is_battery_power_on()) //TODO consider to remove the power check here
				module_state = shutting_down;
			break;

		case shutting_down:
			notify_time_ms = defaults::shutdown_timeout_ms; // in case of sudden powerfail detected
			yellow.pulse<1,24>();
			board.vbus_disable();
			board.power_off();
			if (!is_battery_power_on()) module_state = powered_off; //TODO timeout?
			break;

		case powered_off:
			yellow.set_pwm(2);
			board.vbus_disable();
			if ( !is_battery_power_on()) {
				board.power_on();// release kill signal
				yellow.disable();
				blink(0xff);
				goto_sleep();
				initial_time_ms = 0;
			}
			else {
				yellow.enable();
				blink(0x00);
				module_state = initializing;
				user_initiated_shutdown = false;
			}
			break;

		case initializing:
		default:
			yellow.set_pwm(32);
			if (initial_time_ms < defaults::initial_wait_time_ms) { // need to wait for ADC to stabilize
				board.vbus_enable(); // we need to measure the voltage while there is load
				++initial_time_ms;
			}
			else if (is_battery_power_on()) {
				board.vbus_enable();
				module_state = standby_vbus_low;
				notify_time_ms = 0;
				initial_time_ms = 0;
			}
			else { // insufficient bat voltage
				board.vbus_disable();
				module_state = powered_off;
				initial_time_ms = 0;
			}
			break;
		}

		/* override led if button pressed */
		if (sensors.button_pressed) yellow.set_pwm(255);

		if (check_button_pressed<defaults::power_off_time_ms>())
			user_initiated_shutdown = true;

	}

	template <uint16_t duration>
	bool check_button_pressed(void) {
		if (bpressed_counter >= duration) {
			bpressed_counter = 0;
			return true;
		}
		return false;
	}

	void regulated_vbus(uint16_t target_voltage) {
		if (sensors.voltage_bus <= target_voltage)
			board.vbus_enable();
		else if(sensors.voltage_bus >= target_voltage + defaults::deadband)
			board.vbus_disable();
	}

	void active_bus_regulation(void) {
		if (target_level == 255) board.vbus_enable();
		else regulated_vbus(defaults::vbus_reg_voltage_base + target_level);
	}

	void step(void)
	{
		sensors.step();
		apply_module_state();

		/* prepare status message */
		status_msg =  sensors.button_pressed    << StatusBits::button_pressed
		           |  sensors.limiter_faulted   << StatusBits::limiter_fault
		           |  sensors.battery_charging  << StatusBits::battery_charging
		           |  sensors.charger_connected << StatusBits::charger_connected
		           |  user_initiated_shutdown   << StatusBits::shutdown_initiated
		           |  not is_battery_power_on() << StatusBits::no_battery_inserted
		           |  is_vbus_charging()        << StatusBits::vbus_charging
		           /* battery voltage */
		           | (sensors.voltage_bat >  defaults::vbat_ov_threshold_warning) << StatusBits::vbat_ov_warning
		           | (sensors.voltage_bat <= defaults::vbat_uv_threshold_warning) << StatusBits::vbat_uv_warning
		           | (sensors.voltage_bat <= defaults::vbat_uv_threshold_error  ) << StatusBits::vbat_uv_error
		           /* bus voltage */
		           | (sensors.voltage_bus >  defaults::vbus_ov_threshold_warning) << StatusBits::vbus_ov_warning
		           | (sensors.voltage_bus <= defaults::vbus_uv_threshold_warning) << StatusBits::vbus_uv_warning
		           ;

		/* button hysteresis */
		if ( sensors.button_pressed && bpressed_counter < defaults::power_off_time_ms) ++bpressed_counter;
		if (!sensors.button_pressed && bpressed_counter > 0)                           --bpressed_counter;
	}

	/* this needs to be called by communitcation
	   in order to keep the target voltage up */
	void enable() {
		/* only allowed to reactivate from standby,
		   no host reactivation possibly from power off */
		if (module_state == standby_vbus_low
		 or module_state == active_vbus_high ) {
			no_data_time_ms = 0;
			module_state = active_vbus_high;
		}
	}

	/* conditions */
	bool is_vbus_charging() const { return board.is_vbus_charging(); }
	bool is_battery_power_on() const { return sensors.voltage_bat > defaults::vbat_uv_threshold_error; }
	bool is_battery_power_good() const { return sensors.voltage_bat > defaults::vbat_uv_threshold_warning; }

	bool timeout_no_data_received() const { return no_data_time_ms >= defaults::no_data_timeout_ms; }
	bool timeout_shutdown_clients() const { return notify_time_ms >= defaults::shutdown_timeout_ms; }

	uint16_t get_voltage_bat() const { return sensors.voltage_bat; }
	uint16_t get_voltage_bus() const { return sensors.voltage_bus; }
	uint16_t get_status_bits() const { return status_msg; }
	uint16_t get_time_to_live_s() const { return (defaults::shutdown_timeout_ms - notify_time_ms)/1000; }
	uint8_t  get_state() const { return static_cast<uint8_t>(module_state); }

	void set_voltage_level(uint8_t level) { target_level = level; }

	/* halt all ongoing processes, prepare for bootloader */
	void halt(void) {
		module_state = standby_vbus_low;
		yellow.disable();
		// bootloader is called hereinafter from main()
	}

	void goto_sleep(void) {
		// ubat is adc6 is pcint26
		PCICR |= (1<<PCIE3);
		PCMSK3 |= (1 << PCINT26); // enable pin change interrupt for A6(Ubat), PE2
		set_sleep_mode(SLEEP_MODE_PWR_DOWN);
		sleep_mode(); // sleep now!
	}

};

//ISR(PCINT2_vect){
//}

} /* namespace supreme */

#endif /* SUPREME_ENERGYMODULE_CORE_HPP */
