/* mini simple usart lib for sensorimotor rev_1.2 */


    #define RX0_INTERRUPT		USART0_RX_vect
	#define TXC0_INTERRUPT		USART0_TX_vect
	#define UDRE0_INTERRUPT		USART0_UDRE_vect
	#define UDR0_REGISTER		UDR0
	#define UBRR0L_REGISTER		UBRR0L
	#define UBRR0H_REGISTER		UBRR0H
	#define UCSR0A_REGISTER		UCSR0A
	#define UCSR0B_REGISTER		UCSR0B
	#define UCSR0C_REGISTER		UCSR0C
	#define TXCIE0_BIT      	TXCIE0
	#define UDRIE0_BIT    		UDRIE0
	#define RXCIE0_BIT   		RXCIE0
	#define TXEN0_BIT   		TXEN0
	#define RXEN0_BIT   		RXEN0
	#define UDRE0_BIT   		UDRE0
	#define RXC0_BIT    		RXC0
	#define U2X0_BIT    		U2X0
	#define MPCM0_BIT   		MPCM0
	#define UCSZ02_BIT  		UCSZ02
	#define TXB80_BIT   		TXB80


#define USART0_RS485_MODE


//#define RS485_CONTROL0_DDR // DDRB
//#define RS485_CONTROL0_PORT // PORTB
//#define RS485_CONTROL0_PIN // PINB
//#define RS485_CONTROL0_IONUM // 2 // pin number


class uart
{
public:
    uart();

    reinit();

    write_byte();
    write_buffer();

    read_byte();
    read_buffer();

}


    static inline void uart0_init(uint16_t ubrr_value) // have to be called once at startup
	{
	#ifdef USART0_RS485_MODE
		RS485_CONTROL0_DDR |= (1<<RS485_CONTROL0_IONUM); // default pin state is low
	#endif

		UBRR0L_REGISTER = (uint8_t) ubrr_value;

	#ifdef USART_SKIP_UBRRH_IF_ZERO
		if(__builtin_constant_p(ubrr_value))
			if(((ubrr_value>>8) != 0)) // requires -Os flag - do not use in non-inline functions
	#endif
			UBRR0H_REGISTER = (ubrr_value>>8);

	#ifdef USART0_U2X_SPEED
		#ifdef USART0_MPCM_MODE
			UCSR0A_REGISTER = (1<<U2X0_BIT)|(1<<MPCM0_BIT);
		#else
			UCSR0A_REGISTER = (1<<U2X0_BIT); // enable double speed
		#endif
	#elif defined(USART0_MPCM_MODE)
		UCSR0A_REGISTER |= (1<<MPCM0_BIT);
	#endif

		UCSR0B_REGISTER = USART0_CONFIG_B;
		// 8n1 is set by default, setting UCSRC is not needed

	#ifdef USART0_USE_SOFT_RTS
		RTS0_DDR |= (1<<RTS0_IONUM);
	#endif
	}

void uart_init(unsigned int baudrate)
{
    UART_TxHead = 0;
    UART_TxTail = 0;
    UART_RxHead = 0;
    UART_RxTail = 0;

#ifdef UART_TEST
#ifndef UART0_BIT_U2X
#warning "UART0_BIT_U2X not defined"
#endif
#ifndef UART0_UBRRH
#warning "UART0_UBRRH not defined"
#endif
#ifndef UART0_CONTROLC
#warning "UART0_CONTROLC not defined"
#endif
#if defined(URSEL) || defined(URSEL0)
#ifndef UART0_BIT_URSEL
#warning "UART0_BIT_URSEL not defined"
#endif
#endif
#endif

    /* Set baud rate */
    if ( baudrate & 0x8000 )
    {
        #if UART0_BIT_U2X
        UART0_STATUS = (1<<UART0_BIT_U2X);  //Enable 2x speed
        #endif
    }
    #if defined(UART0_UBRRH)
    UART0_UBRRH = (unsigned char)((baudrate>>8)&0x80) ;
    #endif
    UART0_UBRRL = (unsigned char) (baudrate&0x00FF);

    /* Enable USART receiver and transmitter and receive complete interrupt */
    UART0_CONTROL = _BV(UART0_BIT_RXCIE)|(1<<UART0_BIT_RXEN)|(1<<UART0_BIT_TXEN);

    /* Set frame format: asynchronous, 8data, no parity, 1stop bit */
    #ifdef UART0_CONTROLC
    #ifdef UART0_BIT_URSEL
    UART0_CONTROLC = (1<<UART0_BIT_URSEL)|(1<<UART0_BIT_UCSZ1)|(1<<UART0_BIT_UCSZ0);
    #else
    UART0_CONTROLC = (1<<UART0_BIT_UCSZ1)|(1<<UART0_BIT_UCSZ0);
    #endif
    #endif

}/* uart_init */

