/*---------------------------------+
 | Supreme Machines Energymodule   |
 | Board Layout Configuration      |
 | Jetpack Cognition Lab           |
 | Matthias Kubisch                |
 | kubisch@informatik.hu-berlin.de |
 | Last Modified: May 2021         |
 +---------------------------------*/

#ifndef SUPREME_ENERGYMODULE_HPP
#define SUPREME_ENERGYMODULE_HPP

/* standard libs */
#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/sleep.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/eeprom.h>

/* external libs*/
#include "pin_definitions.hpp"
#include "../uart/uart.hpp"

#ifndef F_CPU
#error "F_CPU undefined, please define CPU frequency in Hz in Makefile"
#endif

#define UART_BAUD_RATE 1000000

namespace supreme {

	namespace adc_channel {
		const uint8_t voltage_bat = 6,
		              voltage_bus = 7;
	}

class energymodule
{
public:

	/* Arduino-like pin names */
	/* A0 */ PIN_TYPE( C, 0) read_disable;   // clear/set rs485 transceiver read mode
	/* A1 */ // ADC1 not in use
	/* A2 */ // ADC2 not in use
	/* A3 */ // ADC3 not in use
	/* A4 */ // ADC4 not in use
	/* A5 */ // ADC5 not in use
	/* A6 */ // ADC6 voltage_bat
	/* A7 */ // ADC7 voltage_bus
	/* RX */ PIN_TYPE( D, 0) rxi;
	/* TX */ PIN_TYPE( D, 1) txo;
	/* D2 */ PIN_TYPE( D, 2) drive_enable;   // set/clear rs485 transceiver write mode
	/* D3 */ PIN_TYPE( D, 3) led_red;
	/* D4 */ PIN_TYPE( D, 4) limiter_enable; // enable voltage for the BUS
	/* D5 */ PIN_TYPE( D, 5) led_yellow;
	/* D6 */ // not in use
	/* D7 */ PIN_TYPE( D, 7) limiter_status; // hi:ok, lo:fault, use pullup
	/* D8 */ PIN_TYPE( B, 0) power_enable;   // pull low to fully power off
	/* D9 */ PIN_TYPE( B, 1) button_status;  // hi: released, lo: pressed

	/* SPI, In-System-Programmer */
	/* D10 */ // CS   not in use
	/* D11 */ // COPI used by ISP
	/* D12 */ // CIPO used by ISP
	/* D13 */ // SCK  used by ISP

	/* Charger Status */
	/* D14 */ PIN_TYPE( E, 0) n_charging;    // active low
	/* D15 */ PIN_TYPE( E, 1) n_power_good;  // active low

	energymodule()
	{
		make_output( led_red        );
		make_output( led_yellow     );

		make_output( limiter_enable );
		make_input ( limiter_status ); set( limiter_status ); // use pullup

		make_output( power_enable   ); set( power_enable );
		make_input ( button_status  ); set( button_status  ); // use pullup

		make_output( txo            );
		make_output( drive_enable   );
		make_output( read_disable   );

		make_input ( n_charging     ); // no pullup used intentionally
		make_input ( n_power_good   ); // no pullup used intentionally

		/* init uart rs485 bus communication at 1Mbaud/s */
		uart_init( UART_BAUD_SELECT(UART_BAUD_RATE, F_CPU) );
		rs485_recv_mode();

		sei(); // enable interrupts

		red_off();
		yellow_off();
	}

	void vbus_enable () { set  (limiter_enable); }
	void vbus_disable() { clear(limiter_enable); }
	bool is_vbus_charging() { return  is_set(limiter_enable); }

	void power_on () { set  (power_enable); }
	void power_off() { clear(power_enable); }

	bool is_button_pressed   () { return !is_set(button_status ); }
	bool is_limiter_faulted  () { return !is_set(limiter_status); }
	bool is_battery_charging () { return !is_set(n_charging    ); }
	bool is_charger_connected() { return !is_set(n_power_good  ); }

	void red_on () { set  (led_red); }
	void red_off() { clear(led_red); }
	void red_toggle() { if( is_set(led_red)) clear(led_red); else set(led_red); }

	void yellow_on () { set  (led_yellow); }
	void yellow_off() { clear(led_yellow); }

	void rs485_send_mode() {
		_delay_us(1);
		set(drive_enable);
		set(read_disable);
		_delay_us(10); // wait at least one bit after enabling the driver
	}

	void rs485_recv_mode() {
		_delay_us(10); // wait at least one bit after disabling the driver
		clear(drive_enable);
		clear(read_disable);
		_delay_us(1);
	}
};

} /* namespace supreme */

#endif /* SUPREME_ENERGYMODULE_HPP */
