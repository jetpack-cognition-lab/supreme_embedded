#include <system/communication.hpp>
#include <board/energymodule.hpp>
#include "./catch_1.10.0.hpp"

#include <test_energymodule_core.hpp>

namespace supreme {
namespace local_tests {


template <typename T>
bool verify_checksum(T const& data) {
	uint8_t sum = 0;
	for (auto const& d : data)
		sum += d;
	return sum == 0;
}


TEST_CASE( "sendbuffer is filled and flushed", "[communication]")
{
	reset_hardware();
	sendbuffer<16> send;

	/* contains sync-bytes */
	REQUIRE( send.size() == 2 );
	REQUIRE(_uart.out_buffer.size() == 0 );

	send.add_byte(0x87);
	REQUIRE( send.size() == 3 );

	send.add_word(0x1234);
	REQUIRE( send.size() == 5 );

	REQUIRE( not _uart.transmit_done );
	board.stats.clear();

	send.flush();
	REQUIRE( _uart.transmit_done );
	REQUIRE( board.stats.send_enable  == 1 );
	REQUIRE( board.stats.recv_enable  == 1 );


	REQUIRE( _uart.out_buffer.size() == 5+1 ); // checksum was added automatically
	REQUIRE( send.size() == 2 ); // sendbuffer was reset

	/* check content of sent data */
	REQUIRE(_uart.out_buffer[0] == 0xff );
	REQUIRE(_uart.out_buffer[1] == 0xff );
	REQUIRE(_uart.out_buffer[2] == 0x87 );
	REQUIRE(_uart.out_buffer[3] == 0x12 );
	REQUIRE(_uart.out_buffer[4] == 0x34 );

	uint8_t chksum = 0;
	for (unsigned i = 0; i <_uart.out_buffer.size() - 1; ++i)
		chksum +=_uart.out_buffer[i];

	chksum = ~chksum + 1;

	REQUIRE( chksum == _uart.out_buffer.back() );

	/* checksum adds to zero */
	uint8_t zero_sum = 0;
	for (auto b : _uart.out_buffer)
		zero_sum += b;

	REQUIRE( zero_sum == 0 );
}

TEST_CASE( "empty sendbuffer is not flushed", "[communication]")
{
	reset_hardware();
	sendbuffer<16> send;

	/* contains sync-bytes */
    REQUIRE( send.size() == 2 );
	REQUIRE( _uart.out_buffer.size() == 0 );

	REQUIRE( not _uart.transmit_done );
	board.stats.clear();

	send.flush();
	REQUIRE( not _uart.transmit_done );
	REQUIRE( board.stats.send_enable == 0 );
	REQUIRE( board.stats.recv_enable == 0 );
	REQUIRE( _uart.out_buffer.size() == 0 ); // nothing is send
}


TEST_CASE( "ping command can be received and is responded", "[communication]")
{
	reset_hardware();

	using core_t = test_energymodule_core;
	using com_t = supreme::communication_ctrl<core_t>;

	core_t ux;
	com_t com(ux);

	REQUIRE( com.get_board_id() == 23 );
	REQUIRE( _uart.in_queue.size() == 0 );
	com.step(); // why does com step not return w/o a byte received
	REQUIRE( _uart.out_buffer.size() == 0 );

	REQUIRE( com.get_state() == com_t::command_state_t::syncing );
	_uart.in_queue.push_back(0xff); // 1st sync
	com.step();

	REQUIRE( com.get_state() == com_t::command_state_t::syncing );
	_uart.in_queue.push_back(0xff); // 2nd sync
	com.step();

	REQUIRE( com.get_state() == com_t::command_state_t::awaiting );
	_uart.in_queue.push_back(0xe0); // ping cmd
	com.step();

	REQUIRE( com.get_state() == com_t::command_state_t::get_id );
	_uart.in_queue.push_back(  23); // motor id
	com.step();

	REQUIRE( com.get_state() == com_t::command_state_t::verifying );
	_uart.in_queue.push_back(0x0B); // checksum
	REQUIRE( not _uart.transmit_done );
	com.step();

	REQUIRE( _uart.in_queue.empty() );
	REQUIRE( com.get_state() == com_t::command_state_t::syncing );
	REQUIRE( com.get_errors() == 0 );

	REQUIRE( _uart.out_buffer.size() == 5 );
	REQUIRE( _uart.transmit_done );

	REQUIRE( _uart.out_buffer[0] == 0xff );
	REQUIRE( _uart.out_buffer[1] == 0xff );
	REQUIRE( _uart.out_buffer[2] == 0xe1 );
	REQUIRE( _uart.out_buffer[3] == 23   );
	REQUIRE( verify_checksum(_uart.out_buffer) );
}

TEST_CASE( "data request command can be received and is responded", "[communication]")
{
	reset_hardware();

	using core_t = test_energymodule_core;
	using com_t = supreme::communication_ctrl<core_t>;

	core_t ux;
	com_t com(ux);

	REQUIRE( com.get_board_id() == 23 );

	com.step();
	REQUIRE( _uart.out_buffer.size() == 0 );

	REQUIRE( com.get_state() == com_t::command_state_t::syncing );
	_uart.in_queue.push_back(0xff); // 1st sync
	com.step();

	REQUIRE( com.get_state() == com_t::command_state_t::syncing );
	_uart.in_queue.push_back(0xff); // 2nd sync
	com.step();

	REQUIRE( com.get_state() == com_t::command_state_t::awaiting );
	_uart.in_queue.push_back(0xC0); // data request
	com.step();

	REQUIRE( com.get_state() == com_t::command_state_t::get_id );
	_uart.in_queue.push_back(  23); // motor id
	com.step();

	REQUIRE( com.get_state() == com_t::command_state_t::verifying );
	_uart.in_queue.push_back(0x2B); // checksum
	REQUIRE( not _uart.transmit_done );
	com.step();

	REQUIRE( _uart.in_queue.empty() );
	REQUIRE( com.get_state() == com_t::command_state_t::syncing );
	REQUIRE( com.get_errors() == 0 );

	REQUIRE( _uart.out_buffer.size() == 16 ); // 15 data plus checksum
	REQUIRE( _uart.transmit_done );

	REQUIRE( _uart.out_buffer[ 0] == 0xff );
	REQUIRE( _uart.out_buffer[ 1] == 0xff );
	REQUIRE( _uart.out_buffer[ 2] == 0x56 ); // raw data response
	REQUIRE( _uart.out_buffer[ 3] == 23   );
	REQUIRE( _uart.out_buffer[ 4] == 10   ); // message length
	REQUIRE( _uart.out_buffer[ 5] == 0x1A ); // ubat
	REQUIRE( _uart.out_buffer[ 6] == 0x1B );
	REQUIRE( _uart.out_buffer[ 7] == 0x2A ); // ubus
	REQUIRE( _uart.out_buffer[ 8] == 0x2B );
	REQUIRE( _uart.out_buffer[ 9] == 0x3A ); // ilim
	REQUIRE( _uart.out_buffer[10] == 0x3B );
	REQUIRE( _uart.out_buffer[11] == 0x4A ); // timetolive
	REQUIRE( _uart.out_buffer[12] == 0x5A ); // state
	REQUIRE( _uart.out_buffer[13] == 0x6A ); // status bits
	REQUIRE( _uart.out_buffer[14] == 0x6B );

	REQUIRE( verify_checksum(_uart.out_buffer) );
}

void send(std::vector<uint8_t> buf) {
	_uart.in_queue.push_back(0xff);
	_uart.in_queue.push_back(0xff);
	uint8_t chksum = 0xfe;
	for (auto& b : buf) {
		chksum += b;
		_uart.in_queue.push_back(b);
	}
	_uart.in_queue.push_back(~chksum + 1);
}

TEST_CASE( "valid commands and responses for other motors is ignored", "[communication]")
{
	using core_t = test_energymodule_core;
	using com_t = supreme::communication_ctrl<core_t>;

	/* msg for different motor id (to be ignored) */
	std::vector<uint8_t> ping         = { 0xe0, 42 };
	std::vector<uint8_t> data_request = { 0xC0, 43 };
	std::vector<uint8_t> set_id       = { 0x70, 44, 13 };

	/* msg responses from different motor ids */
	std::vector<uint8_t> re_ping         = { 0xe1, 42 };
	std::vector<uint8_t> re_data_request = { 0x80, 43, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	std::vector<uint8_t> re_set_id       = { 0x71, 13 };

	reset_hardware();
	core_t ux;
	com_t com(ux);
	REQUIRE( com.get_board_id() == 23 );

	for (auto const& cmd : { ping
	                       , data_request
	                       , set_id
	                       , re_ping
	                       , re_data_request
	                       , re_set_id
	                       } )
	{
		reset_hardware();
		com.step();
		REQUIRE( _uart.out_buffer.size() == 0 );
		REQUIRE( _uart.in_queue.empty() );
		REQUIRE( com.get_state() == com_t::command_state_t::syncing );

		send(cmd);

		REQUIRE( not _uart.transmit_done );
		com.step();

		REQUIRE( _uart.in_queue.empty() );
		REQUIRE( com.get_state() == com_t::command_state_t::syncing );
		REQUIRE( com.get_errors() == 0 );
		REQUIRE( _uart.out_buffer.size() == 0 );
		REQUIRE( not _uart.transmit_done );
	}
}

TEST_CASE( "set_id command can be received, id is set and command is responded", "[communication]")
{
	reset_hardware();

	using core_t = test_energymodule_core;
	using com_t = supreme::communication_ctrl<core_t>;

	core_t ux;
	com_t com(ux);

	REQUIRE( com.get_board_id() == 23 );
	uint8_t new_id = 1;

	com.step();
	REQUIRE( _uart.out_buffer.size() == 0 );

	REQUIRE( com.get_state() == com_t::command_state_t::syncing );
	_uart.in_queue.push_back(0xff); // 1st sync
	com.step();

	REQUIRE( com.get_state() == com_t::command_state_t::syncing );
	_uart.in_queue.push_back(0xff); // 2nd sync
	com.step();

	REQUIRE( com.get_state() == com_t::command_state_t::awaiting );
	_uart.in_queue.push_back(0x70); // set_id cmd
	com.step();

	REQUIRE( com.get_state() == com_t::command_state_t::get_id );
	_uart.in_queue.push_back(  23); // motor id
	com.step();

	REQUIRE( com.get_state() == com_t::command_state_t::reading );
	_uart.in_queue.push_back(new_id); // motor id
	com.step();

	REQUIRE( com.get_state() == com_t::command_state_t::verifying );
	_uart.in_queue.push_back(0x7A); // checksum
	REQUIRE( not _uart.transmit_done );
	com.step();

	REQUIRE( _uart.in_queue.empty() );
	REQUIRE( com.get_state() == com_t::command_state_t::syncing );
	REQUIRE( com.get_errors() == 0 );

	REQUIRE( _uart.out_buffer.size() == 5 );
	REQUIRE( _uart.transmit_done );

	REQUIRE( com.get_board_id() == new_id );

	REQUIRE( _uart.out_buffer[0] == 0xff );
	REQUIRE( _uart.out_buffer[1] == 0xff );
	REQUIRE( _uart.out_buffer[2] == 0x71 );
	REQUIRE( _uart.out_buffer[3] == new_id);
	REQUIRE( verify_checksum(_uart.out_buffer) );
}

TEST_CASE( "invalid set_id command is refused when checksum is wrong", "[communication]")
{
	reset_hardware();
	set_motor_id(1);

	using core_t = test_energymodule_core;
	using com_t = supreme::communication_ctrl<core_t>;

	core_t ux;
	com_t com(ux);

	REQUIRE( com.get_board_id() == 1 );
	uint8_t new_id = 42;

	com.step();
	REQUIRE( _uart.out_buffer.size() == 0 );
	REQUIRE( _uart.in_queue.size() == 0);
	_uart.in_queue.push_back(0xff);   // 1st sync
	_uart.in_queue.push_back(0xff);   // 2nd sync
	_uart.in_queue.push_back(0x70);   // set_id cmd
	_uart.in_queue.push_back(   1);   // old motor id
	_uart.in_queue.push_back(new_id); // new motor id
	_uart.in_queue.push_back(0x66);   // invalid checksum

	com.step();

	REQUIRE( com.get_board_id() != new_id );
	REQUIRE( com.get_board_id() == 1 );
	REQUIRE( com.get_errors() == 1 );
	REQUIRE( _uart.in_queue.size() == 0);
	REQUIRE( _uart.out_buffer.size() == 0 );

}

TEST_CASE( "invalid set_id command is refused when new id is wrong", "[communication]")
{
	using core_t = test_energymodule_core;
	using com_t = supreme::communication_ctrl<core_t>;
	core_t ux;

	std::vector<uint8_t> wrong0 = { 0x70, 1, 128  };
	std::vector<uint8_t> wrong1 = { 0x70, 1, 255  };

	std::vector<uint8_t> correct0 = { 0x70, 1, 13  };
	std::vector<uint8_t> correct1 = { 0x70, 1, 127 };
	std::vector<uint8_t> correct2 = { 0x70, 1, 0   };

	for (auto const& cmd : { correct0, correct1, correct2} ) {
		reset_hardware();
		set_motor_id(1);
		com_t com(ux);
		REQUIRE( com.get_board_id() == 1 );

		send(cmd);
		com.step();
		REQUIRE( com.get_errors() == 0 );
		REQUIRE( com.get_board_id() == cmd[2] );
	}

	for (auto const& cmd : { wrong0, wrong1} ) {
		reset_hardware();
		set_motor_id(1);
		com_t com(ux);
		REQUIRE( com.get_board_id() == 1 );

		send(cmd);
		com.step();
		REQUIRE( com.get_errors() == 1 );
		REQUIRE( com.get_board_id() != cmd[2] );
		REQUIRE( com.get_board_id() == 1 );
	}
}

TEST_CASE( "multiple pings", "[communication]")
{
	using core_t = test_energymodule_core;
	using com_t = supreme::communication_ctrl<core_t>;

	/* msg for different motor id (to be ignored) */
	std::vector<uint8_t> ping     = { 0xe0, 42 };

	/* msg responses from different motor ids */
	std::vector<uint8_t> re_ping  = { 0xe1, 42 };

	reset_hardware();
	set_motor_id(23);
	core_t ux;
	com_t com(ux);

	REQUIRE( com.get_board_id() == 23 );

	for (unsigned id = 0; id < 128; ++id)
	{
		_uart.out_buffer.clear();
		_uart.transmit_done = false;

		REQUIRE( _uart.out_buffer.size() == 0 );
		REQUIRE( _uart.in_queue.empty() );
		REQUIRE( com.get_state() == com_t::command_state_t::syncing );

		ping[1] = id;
		send(ping);
		if (id != 23) {
			re_ping[1] = id;
			send(re_ping);
		}

		REQUIRE( not _uart.transmit_done );
		com.step();

		REQUIRE( _uart.in_queue.empty() );
		REQUIRE( com.get_state() == com_t::command_state_t::syncing );
		REQUIRE( com.get_errors() == 0 );

		if (id != 23) {
			REQUIRE( _uart.out_buffer.size() == 0 );
			REQUIRE( not _uart.transmit_done );
		} else {
			REQUIRE( _uart.out_buffer.size() == 5 );
			REQUIRE( _uart.transmit_done );
		}
	}
}


TEST_CASE( "multiple data requests", "[communication]")
{
	using core_t = test_energymodule_core;
	using com_t = supreme::communication_ctrl<core_t>;

	/* msg for different motor id (to be ignored) */
	std::vector<uint8_t> data_request     = { 0xC0, 42 };

	/* msg responses from different motor ids */
	std::vector<uint8_t> re_data_request  = { 0x80, 43, 0, 1, 2, 0xff, 0xff, 0xC0, 6, 7, 8, 9 };

	reset_hardware();
	set_motor_id(23);
	core_t ux;
	com_t com(ux);

	REQUIRE( com.get_board_id() == 23 );

	for (unsigned id = 0; id < 128; ++id)
	{
		_uart.out_buffer.clear();
		_uart.transmit_done = false;

		REQUIRE( _uart.out_buffer.size() == 0 );
		REQUIRE( _uart.in_queue.empty() );
		REQUIRE( com.get_state() == com_t::command_state_t::syncing );

		data_request[1] = id;
		send(data_request);
		if (id != 23) {
			re_data_request[1] = id;
			send(re_data_request);
		}

		REQUIRE( not _uart.transmit_done );
		com.step();

		REQUIRE( _uart.in_queue.empty() );
		REQUIRE( com.get_state() == com_t::command_state_t::syncing );
		REQUIRE( com.get_errors() == 0 );

		if (id != 23) {
			REQUIRE( _uart.out_buffer.size() == 0 );
			REQUIRE( not _uart.transmit_done );
		} else {
			REQUIRE( _uart.out_buffer.size() == 16 );
			REQUIRE( _uart.transmit_done );
		}
	}
}

TEST_CASE( "find valid messages in garbage", "[communication]")
{
	using core_t = test_energymodule_core;
	using com_t = supreme::communication_ctrl<core_t>;

	std::vector<uint8_t> ping         = { 0xe0, 23 };
	std::vector<uint8_t> data_request = { 0xC0, 23 };
	std::vector<uint8_t> set_id       = { 0x70, 23, 23 };

	/* msg responses from different motor ids */
	std::vector<uint8_t> re_ping         = { 0xe1, 42 };
	std::vector<uint8_t> re_data_request = { 0x80, 43, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	std::vector<uint8_t> re_set_id       = { 0x71, 13 };

	std::vector<uint8_t> garbage      = { 0xff, 0xdd, 0xff, 0x34, 0xe1, 23, 0xff, 0xfe, 0x03 };

	reset_hardware();
	set_motor_id(23);
	core_t ux;
	com_t com(ux);

	REQUIRE( com.get_board_id() == 23 );

	for (auto const& cmd : { ping
	                       , data_request
	                       , set_id
	                       , re_ping
	                       , re_data_request
	                       , re_set_id
	                       } )
	{
		for (auto& g: garbage) _uart.in_queue.push_back(g);
		send(cmd);
	}

	_uart.out_buffer.clear();
	_uart.transmit_done = false;

	REQUIRE( com.get_state() == com_t::command_state_t::syncing );

	REQUIRE( not _uart.transmit_done );
	com.step();

	REQUIRE( _uart.in_queue.empty() );
	REQUIRE( com.get_state() == com_t::command_state_t::syncing );
	REQUIRE( com.get_errors() == 0 );

	REQUIRE( _uart.out_buffer.size() == 5 + 16 + 5/* TODO: detect cmd response */ );
	REQUIRE( _uart.transmit_done );
}

TEST_CASE( "set_voltage command can be received, level is set and command is responded with data", "[communication]")
{
	reset_hardware();

	using core_t = test_energymodule_core;
	using com_t = supreme::communication_ctrl<core_t>;

	core_t ux;
	com_t com(ux);

	REQUIRE( com.get_board_id() == 23 );
	uint8_t new_id = 1;

	std::vector<uint8_t> set_voltage_cmd = { 0xB1, 23, 64 };

	reset_hardware();
	com.step();
	REQUIRE( _uart.out_buffer.size() == 0 );
	REQUIRE( _uart.in_queue.empty() );
	REQUIRE( com.get_state() == com_t::command_state_t::syncing );

	send(set_voltage_cmd);

	REQUIRE( not _uart.transmit_done );

	REQUIRE( ux.voltage_level == 0 );

	com.step();

	REQUIRE( ux.voltage_level == 64 );

	REQUIRE( _uart.in_queue.empty() );
	REQUIRE( com.get_state() == com_t::command_state_t::syncing );
	REQUIRE( com.get_errors() == 0 );

	/* received data package */
	REQUIRE( _uart.out_buffer.size() == 16 );
	REQUIRE( _uart.out_buffer[2] == 0x56 );
	REQUIRE( _uart.out_buffer[3] == 23 );
	REQUIRE( _uart.transmit_done );
}

int16_t get_signed_word(uint8_t hi, uint8_t lo) { return (hi << 8) | lo; }

}} /* namespace supreme::local_tests */
