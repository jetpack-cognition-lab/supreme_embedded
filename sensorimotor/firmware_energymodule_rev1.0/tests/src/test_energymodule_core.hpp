namespace supreme {
namespace local_tests {

class test_energymodule_core {
public:

	void step() { }

	void set_voltage_level(uint8_t level) { voltage_level = level; }

	void enable()  { enabled = true; }
	void disable() { enabled = false; }

	uint16_t get_voltage_bat() const { return 0x1A1B; }
	uint16_t get_voltage_bus() const { return 0x2A2B; }
	uint16_t get_current_lim() const { return 0x3A3B; }

	uint8_t  get_time_to_live_s() const { return 0x4A; }
	uint8_t  get_state() const { return 0x5A; }
	uint16_t get_status_bits() const { return 0x6A6B; }

	uint8_t voltage_level = 0;
	bool    enabled   = false;

};

}} /* namespace supreme::local_tests */
