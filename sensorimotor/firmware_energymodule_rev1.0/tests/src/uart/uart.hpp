#ifndef TEST_UART_HPP
#define TEST_UART_HPP

#include <stdio.h>
#include <stdint.h>
#include <vector>
#include <queue>

class uartTest {
public:

	std::vector<uint8_t> out_buffer;
	std::deque<uint8_t>  in_queue;
	bool transmit_done = false;

	uartTest() : out_buffer(), in_queue() {}
}
	_uart;

bool transmission_complete(void) { return _uart.transmit_done; }

uint16_t uart_getc(void) {
	if (_uart.in_queue.empty()) return 0x0100; // UART NO DATA
	uint8_t read_byte = _uart.in_queue.front();
	_uart.in_queue.pop_front();
	return read_byte;
}


void uart_putc(uint8_t b) {
	_uart.out_buffer.emplace_back(b);
	_uart.transmit_done = true; //TODO right?
}

#endif /* TEST_UART */
