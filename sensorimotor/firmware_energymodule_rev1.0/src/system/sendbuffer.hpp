/*---------------------------------+
 | Jetpack Cognition Lab, Inc.     |
 | Supreme Machines                |
 | Energymodule Firmware           |
 | Matthias Kubisch                |
 | kubisch@informatik.hu-berlin.de |
 | May 2021                        |
 +---------------------------------*/

#ifndef SUPREME_SENDBUFFER_HPP
#define SUPREME_SENDBUFFER_HPP

#include <board/energymodule.hpp>
#include <system/assert.hpp>
#include <system/constants.hpp>

extern supreme::energymodule board; // find better way

namespace supreme {

template <unsigned N>
class sendbuffer {

	static constexpr unsigned NumSyncBytes = 2;
	static constexpr uint8_t chk_init = (constants::syncbyte + constants::syncbyte) % 256;
	uint16_t  ptr = NumSyncBytes;
	uint8_t   buffer[N];
	uint8_t   checksum = chk_init;

public:
	sendbuffer()
	{
		static_assert(N > NumSyncBytes, "Invalid buffer size.");
		for (uint8_t i = 0; i < NumSyncBytes; ++i)
			buffer[i] = constants::syncbyte; // init sync bytes once
	}

	void add_byte(uint8_t byte) {
		assert(ptr < (N-1), constants::assertion::buffer_not_exceeded);
		buffer[ptr++] = byte;
		checksum += byte;
	}

	void add_word(uint16_t word) {
		add_byte((word  >> 8) & constants::syncbyte);
		add_byte( word        & constants::syncbyte);
	}

	void discard(void) { ptr = NumSyncBytes; }

	void flush() {
		if (ptr == NumSyncBytes) return;
		add_checksum();
		board.rs485_send_mode(); // TODO: let the uart lib control the rs485 enable/disable

		for (uint8_t i = 0; i < ptr; ++i)
			uart_putc(buffer[i]);

		while(!transmission_complete());
		//TODO: consider not waiting here, but set a flag and check in com step to switch into recv mode again
		board.rs485_recv_mode();
		/* prepare next */
		ptr = NumSyncBytes;
	}

	uint16_t size(void) const { return ptr; }

private:

	void add_checksum() {
		assert(ptr < N, constants::assertion::buffer_not_exceeded);
		buffer[ptr++] = ~checksum + 1; /* two's complement checksum */
		checksum = chk_init;
	}

};

} /* namespace supreme */

#endif /* SUPREME_SENDBUFFER_HPP */
