/*---------------------------------+
 | Jetpack Cognition Lab, Inc.     |
 | Supreme Machines                |
 | Energymodule Firmware           |
 | Matthias Kubisch                |
 | kubisch@informatik.hu-berlin.de |
 | July 2021                       |
 +---------------------------------*/

#ifndef SUPREME_ENERGYMODULE_CORE_HPP
#define SUPREME_ENERGYMODULE_CORE_HPP

#ifndef min
#define min(a,b) (((a) < (b)) ? (a) : (b))
#endif

#include <avr/power.h>
#include <util/atomic.h>

#include <board/energymodule.hpp>
#include <system/adc.hpp>
#include <system/assert.hpp>
#include <system/ledpwm.hpp>

extern supreme::energymodule board;
volatile uint16_t global_counter_s = 0;

namespace supreme {

enum StatusBits : uint8_t
{
	button_pressed      =  0,
	charger_connected   =  1,
	battery_charging    =  2,
	limiter_fault       =  3,
	vbus_charging       =  4,
	no_battery_inserted =  5,
	host_init_shutdown  =  6,
	user_init_shutdown  =  7,
	vbat_ov_warning     =  8,
	vbat_uv_warning     =  9,
	vbat_uv_error       = 10,
	vbus_ov_warning     = 11,
	vbus_uv_warning     = 12,
//	reserved_13         = 13,
//	reserved_14         = 14,
//	reserved_15         = 15,
};

enum ModuleState : uint8_t
{
	initializing   = 0,
	charge_capacitor  ,
	powered_off       ,
	shutting_down     ,
	notify_subsystems ,
	standby_vbus_low  ,
	active_vbus_high  ,
};

namespace defaults {

	using constants::unit_adc;

	const uint16_t shutdown_timeout_ms = 10000; // 10 s
	const uint16_t no_data_timeout_ms = 100;
	const uint16_t inactivity_timeout_s = 300; // 5min

	/* button times */
	const uint16_t power_off_time_ms = 2000; // 2 s
	const uint16_t initial_wait_time_ms = 250;

	/*---------------------------------+
	 |             56    1023          |
	 |   Vbat0 = ----- x ---- x Vbat   |
	 |           18+56   3V3           |
	 +---------------------------------*/

	constexpr float    unit_vbat = unit_adc * 56.f / (18.f + 56.f);
	constexpr uint16_t vbat_ov_threshold_warning = 4.2f * unit_vbat;
	constexpr uint16_t vbat_uv_threshold_warning = 3.2f * unit_vbat;
	constexpr uint16_t vbat_uv_threshold_error   = 2.6f * unit_vbat;
	constexpr uint16_t vbat_off_threshold        = 1.2f * unit_vbat;

	/*---------------------------------+
	 |             68    1023          |
	 |   Vbus0 = ----- x ---- x Vbus   |
	 |           56+68   3V3           |
	 +---------------------------------*/

	constexpr float    unit_vbus = unit_adc * 68.f / (56.f + 68.f);
	constexpr uint16_t vbus_ov_threshold_warning = 6.0f * unit_vbus;
	constexpr uint16_t vbus_uv_threshold_warning = 4.0f * unit_vbus;

	constexpr uint16_t vbus_active_voltage       = 5.915f * unit_vbus;
	constexpr uint16_t vbus_reg_voltage_base     = vbus_active_voltage-255; // leave 255 (~1,5V) for regulation, see below
	constexpr uint16_t vbus_standby_voltage      = 5.0f * unit_vbus;
	constexpr uint16_t vbus_shutdown_voltage     = 4.0f * unit_vbus;
	constexpr uint16_t vbus_openmain_voltage     = 4.2f * unit_vbus;

	constexpr uint16_t R_ilm = 1000; // Ohm
	constexpr uint16_t unit_vbat_to_p1mV = round(10000.0f / unit_vbat);
	constexpr uint16_t unit_vbus_to_p1mV = round(10000.0f / unit_vbus);
	constexpr uint16_t unit_ilim_to_p1mA = round(10000.0f * 1e6f / (unit_adc * 656.0f * R_ilm));

	const uint8_t deadband = 2;
}

class Sensors {
public:
	/*                              V_ilm(mV)
	   I_out(mA) = G_imon (µA/A) x -----------
	                               R_ilm (Ohm)

	G_imon = 656
	R_ilm = 1000

	V_ilm (mV)

	*/


	float voltage_bat = .0f;
	float voltage_bus = .0f;
	float limiter_cur = .0f;

	bool button_pressed    = false;
	bool limiter_faulted   = false;
	bool charger_connected = false;
	bool battery_charging  = false;

	void step(void)
	{
		/* buffer the inputs state for this cycle */

		voltage_bat       = .95f*voltage_bat + .05f*adc::result[adc::voltage_bat]; // simple IIR low-pass filter
		voltage_bus       = .95f*voltage_bus + .05f*adc::result[adc::voltage_bus];
		limiter_cur       = .95f*limiter_cur + .05f*adc::result[adc::limiter_cur];

		button_pressed    = board.is_button_pressed();
		limiter_faulted   = board.is_limiter_faulted();
		charger_connected = board.is_charger_connected();
		battery_charging  = board.is_battery_charging();
	}
};



class energymodule_core {

	ModuleState      module_state = initializing;
	Sensors          sensors;
	Pulsed_LED       yellow;

	uint16_t         no_data_time_ms = 0;
	uint16_t         initial_time_ms = 0;
	uint16_t          notify_time_ms = 0;

	uint16_t         status_msg = 0;
	uint16_t         bpressed_counter = 0;
	uint8_t          target_level = 0;

	bool             host_initiated_shutdown = false;
	bool             user_initiated_shutdown = false;

public:

	energymodule_core()
	: sensors()
	, yellow()
	{
		yellow.enable_pwm();

		/* setup auto-shutdown timer to call compare match each second */
		TCCR1A = (1<<WGM12);             // CTC mode
		TCCR1B = (1<<CS12) | (1<<CS12);  // set prescaler to 1024
		OCR1A = 15624;                   // set timer compare register to 15625-1
		TIMSK1 = (1<<OCIE1A);            // enable compare interrupt

	}

	bool is_shutdown_condition(void) {
		return user_initiated_shutdown    // user pressed power button
		    or host_initiated_shutdown    // host has requested shutdown
		    or !is_battery_power_on()     // user enforced hard power off, or battery removed
		    or timeout_module_inactive()  // no data for quite some time
		;
	}

	bool is_standby_condition(void) {
		return timeout_no_data_received()
		    or sensors.battery_charging
		    or sensors.charger_connected
		;
	}

	void apply_module_state(void)
	{
		switch(module_state)
		{
		case active_vbus_high:
			yellow.set_pwm(64);
			active_bus_regulation();
			reset_global_counter();
			if (no_data_time_ms < defaults::no_data_timeout_ms) no_data_time_ms++;
			if (is_standby_condition ()) module_state = standby_vbus_low;
			if (is_shutdown_condition()) module_state = notify_subsystems;
			break;

		case standby_vbus_low:
			yellow.idle();
			regulated_vbus(defaults::vbus_standby_voltage);
			no_data_time_ms = 0;
			if (is_shutdown_condition()) {
				module_state = notify_subsystems;
			}
			if (is_cap_power_good() and !board.is_main_open())
				board.open_main();
			else if (is_cap_power_low())
				board.close_main();
			break;

		case notify_subsystems:
			yellow.pulse<2,24>();
			++notify_time_ms;
			regulated_vbus(defaults::vbus_shutdown_voltage);
			if (timeout_shutdown_clients() or is_cap_power_low())
				module_state = shutting_down;
			break;

		case shutting_down:
			notify_time_ms = defaults::shutdown_timeout_ms; // in case of sudden powerfail detected
			//yellow.pulse<1,24>();
			board.close_main();
			board.vbus_disable();
			if (not board.is_main_open())
				module_state = powered_off;
			break;

		case powered_off:
			yellow.disable_pwm();
			board.vbus_disable();
			board.battery_charging_on();
			//suspend( /* instant = */ !is_battery_power_on() );
			suspend();
			break;

		case charge_capacitor:
			if (is_cap_power_good()) {
				board.battery_charging_on(); // allow battery charging again
				module_state = standby_vbus_low;
			}
			if (is_shutdown_condition()) {
				board.vbus_disable();
				module_state = powered_off;
				initial_time_ms = 0;
			}
			if (sensors.limiter_faulted) {
				board.vbus_disable();
				yellow.set_pwm(128);
				board.red_on();
			}
			else {
				board.vbus_enable();
				yellow.pulse<0,48>();
				board.red_off();
			}
			break;

		case initializing:
		default:
			yellow.set_pwm(64);
			if (initial_time_ms < defaults::initial_wait_time_ms) { // need to wait for ADC to stabilize
				board.vbus_enable(); // we need to measure the voltage while there is load
				++initial_time_ms;
			}
			else if (is_battery_power_on()) {
				board.vbus_enable();
				module_state = charge_capacitor;
				notify_time_ms = 0;
				initial_time_ms = 0;
			}
			else { // insufficient bat voltage
				board.vbus_disable();
				module_state = powered_off;
				initial_time_ms = 0;
			}
			break;
		}

		/* override led if button pressed */
		if (sensors.button_pressed) {
			yellow.set_pwm(255);
			reset_global_counter();
		}

		if (check_button_pressed<defaults::power_off_time_ms>())
			user_initiated_shutdown = true;

	}

	template <uint16_t duration>
	bool check_button_pressed(void) {
		if (bpressed_counter >= duration) {
			bpressed_counter = 0;
			return true;
		}
		return false;
	}

	void regulated_vbus(uint16_t target_voltage) {
		if (sensors.voltage_bus <= target_voltage)
			board.vbus_enable();
		else if(sensors.voltage_bus >= target_voltage + defaults::deadband)
			board.vbus_disable();
	}

	void active_bus_regulation(void) {
		if (target_level == 255) board.vbus_enable();
		else regulated_vbus(defaults::vbus_reg_voltage_base + target_level);
	}

	void step(void)
	{
		sensors.step();
		apply_module_state();

		/* prepare status message */
		status_msg =  sensors.button_pressed    << StatusBits::button_pressed
		           |  sensors.limiter_faulted   << StatusBits::limiter_fault
		           |  sensors.battery_charging  << StatusBits::battery_charging
		           |  sensors.charger_connected << StatusBits::charger_connected
		           |  host_initiated_shutdown   << StatusBits::host_init_shutdown
		           |  user_initiated_shutdown   << StatusBits::user_init_shutdown
		           |  not is_battery_power_on() << StatusBits::no_battery_inserted
		           |  is_vbus_charging()        << StatusBits::vbus_charging
		           /* battery voltage */
		           | (sensors.voltage_bat >  defaults::vbat_ov_threshold_warning) << StatusBits::vbat_ov_warning
		           | (sensors.voltage_bat <= defaults::vbat_uv_threshold_warning) << StatusBits::vbat_uv_warning
		           | (sensors.voltage_bat <= defaults::vbat_uv_threshold_error  ) << StatusBits::vbat_uv_error
		           /* bus voltage */
		           | (sensors.voltage_bus >  defaults::vbus_ov_threshold_warning) << StatusBits::vbus_ov_warning
		           | (sensors.voltage_bus <= defaults::vbus_uv_threshold_warning) << StatusBits::vbus_uv_warning
		           ;

		/* button hysteresis */
		if ( sensors.button_pressed && bpressed_counter < defaults::power_off_time_ms) ++bpressed_counter;
		if (!sensors.button_pressed && bpressed_counter > 0)                           --bpressed_counter;
	}

	/* this needs to be called by communitcation
	   in order to keep the target voltage up */
	void enable() {
		/* only allowed to reactivate from standby,
		   no host reactivation possibly from power off */
		if ((module_state == standby_vbus_low
		 or module_state == active_vbus_high)
		and !is_standby_condition()) {
			no_data_time_ms = 0;
			module_state = active_vbus_high;
		}
	}

	/* conditions */
	bool is_vbus_charging() const { return board.is_vbus_charging(); }
	bool is_battery_off() const { return sensors.voltage_bat < defaults::vbat_off_threshold; }
	bool is_battery_power_on() const { return sensors.voltage_bat > defaults::vbat_uv_threshold_error; }
	bool is_battery_power_good() const { return sensors.voltage_bat > defaults::vbat_uv_threshold_warning; }

	bool timeout_no_data_received() const { return no_data_time_ms >= defaults::no_data_timeout_ms; }
	bool timeout_shutdown_clients() const { return notify_time_ms >= defaults::shutdown_timeout_ms; }

	bool timeout_module_inactive()  const {
		bool is_inactive = false;
		ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
			is_inactive = global_counter_s >= defaults::inactivity_timeout_s;
		}
		return is_inactive;
	}

	void reset_global_counter() {
		ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
			global_counter_s = 0;
		}
	}

	bool is_cap_power_good() const { return sensors.voltage_bus > defaults::vbus_openmain_voltage; }
	bool is_cap_power_low() const { return sensors.voltage_bus < defaults::vbus_shutdown_voltage; }

	uint16_t get_voltage_bat() const { return sensors.voltage_bat * defaults::unit_vbat_to_p1mV; }
	uint16_t get_voltage_bus() const { return sensors.voltage_bus * defaults::unit_vbus_to_p1mV; }
	uint16_t get_current_lim() const { return sensors.limiter_cur * defaults::unit_ilim_to_p1mA; }
	uint16_t get_status_bits() const { return status_msg; }
	uint8_t  get_time_to_live_s() const { return (defaults::shutdown_timeout_ms - notify_time_ms)/1000; }
	uint8_t  get_state() const { return static_cast<uint8_t>(module_state); }

	void set_voltage_level(uint8_t level) {
		if (0 == level) // host requests shutdown
			host_initiated_shutdown = true;
		target_level = level;
	}

	/* halt all ongoing processes, prepare for bootloader */
	void halt(void) {
		module_state = standby_vbus_low;
		yellow.disable_pwm();
		// bootloader is called hereinafter from main()
	}

	static void suspend(bool instant = false);
private:
	static void self_power_off(void);
};

void energymodule_core::suspend(bool instant)
{
	board.red_off();
	supreme::adc::disable();
	wdt_disable();

	if (!instant)
		blink(0x0);
	board.yellow_on();

	PCICR &= ~(1<<PCIE3);
	PCMSK3 &= ~(1 << PCINT26); // disable pin change interrupt for A6(Ubat), PE2

	PCICR &= ~(1<<PCIE0);
	PCMSK0 &= ~(1 << PCINT1); // disable pin change interrupt for D9(INT), PB1

	self_power_off();
	if (!instant)
		_delay_ms(3000); // wait for power drop to settle
	board.yellow_off();
	set_sleep_mode(SLEEP_MODE_PWR_DOWN);

	cli();
	const uint8_t mcucr1 = MCUCR | _BV(BODS) | _BV(BODSE); // turn off BOD during sleep
	const uint8_t mcucr2 = mcucr1 & ~_BV(BODSE);
	MCUCR = mcucr1;
	MCUCR = mcucr2;

	/* pin change enable should be done after power drop */
	PCICR |= (1<<PCIE3);
	PCICR |= (1<<PCIE0);

	PCMSK3 |= (1 << PCINT26); // enable pin change interrupt for A6(Ubat), PE2
	PCMSK0 |= (1 << PCINT1); // enable pin change interrupt for D9(INT), PB1

	// reduce clock
	CLKPR = (1 << CLKPCE); // Enable change of CLKPS bits
	CLKPR = 3; // Set prescaler to 8

	sei();

	sleep_mode(); // sleep now!

	/* we wake-up to self-reset and will not continue here */
	stop(constants::assertion::unexpected_wakeup);
}

void energymodule_core::self_power_off(void) {
	board.power_off();
	_delay_us(50);
	board.power_on(); // release kill signal
}


ISR(PCINT3_vect) {
	// even if empty, do NOT remove!
	PCICR &= ~(1<<PCIE3);
	PCMSK3 &= ~(1 << PCINT26); // disable pin change interrupt for A6(Ubat), PE2
	wdt_enable(WDTO_15MS); // soft reset
	while(true);
}

ISR(PCINT0_vect) {
	// even if empty, do NOT remove!
	PCICR &= ~(1<<PCIE0);
	PCMSK0 &= ~(1 << PCINT1); // disable pin change interrupt for D9(INT), PB1
	wdt_enable(WDTO_15MS); // soft reset
	while(true);
}

/* this is called once TCNT1 = OCR1A    *
 * resulting in a 1 s cycle time, 1 Hz */
ISR (TIMER1_COMPA_vect)
{
	++global_counter_s;
}


/*
	consider a sleep mode with timer...
	each second the device wakes and checks the 3v3 supply voltage, if that is lower than bod
	then the kill pin is NOT released (prevent power on) and the device must wait until the voltage drops below 1.0
*/

} /* namespace supreme */

#endif /* SUPREME_ENERGYMODULE_CORE_HPP */
