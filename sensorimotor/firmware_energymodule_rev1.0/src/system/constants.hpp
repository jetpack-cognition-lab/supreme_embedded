/*---------------------------------+
 | Jetpack Cognition Lab, Inc.     |
 | Supreme Machines Sensorimotor   |
 | Firmware for Board-Revision 1.2 |
 | Author: Matthias Kubisch        |
 | kubisch@informatik.hu-berlin.de |
 | Last Update: January 2021       |
 +---------------------------------*/

#ifndef SUPREME_CONSTANTS_HPP
#define SUPREME_CONSTANTS_HPP

namespace supreme {

namespace constants {

	constexpr uint8_t max_id = 0x7F;
	constexpr uint8_t syncbyte = 0xFF;

	constexpr uint8_t LF = 0x4F;
	constexpr uint8_t HF = 0xDD;
	constexpr uint8_t EF = 0xF5;

	enum class assertion {
		process_command       = 0x11, // 0001.0001
		waiting_for_id        = 0x22, // 0010.0010
		waiting_for_data      = 0x33, // 0011.0011
		eating_others_data    = 0x44, // 0100.0100
		buffer_not_exceeded   = 0x55, // 0101.0101
		unknown_command_state = 0x66, // 0110.0110
		single_byte_commands  = 0x77, // 0111.0111
		no_sync_in_finished   = 0x88, // 1000.1000
		wrong_fuse_bits_set   = 0xCC, // 1100.1100
		unexpected_wakeup     = 0xDD, // 1101.1101
	};

	constexpr float unit_adc = 1023.0f/3.3f;
	constexpr uint16_t stable_voltage_threshold_mV = 3135; // 95% of 3V3

} /* namespace constants */

} /* namespace supreme */

#endif /* SUPREME_CONSTANTS_HPP */
