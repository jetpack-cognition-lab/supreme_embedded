/*---------------------------------+
 | Jetpack Cognition Lab, Inc.     |
 | Supreme Machines                |
 | Energymodule Firmware           |
 | Matthias Kubisch                |
 | kubisch@informatik.hu-berlin.de |
 | May 2021                        |
 +---------------------------------*/

#include <board/energymodule.hpp>
#include <system/core.hpp>
#include <system/communication.hpp>
#include <system/adc.hpp>
#include <system/bootloader.hpp>
#include <system/fuses.hpp>
#include <system/read_vcc.hpp>
#include <system/constants.hpp>

supreme::energymodule board; /* initialize energymodule board */

/* Disable all previous watchdog timer activities of the
   bootloader, in order to not interfere with the firmware.*/
void wdt_init(void) __attribute__((naked)) __attribute__((section(".init3")));

void wdt_init(void) {
	MCUSR &= ~(_BV(WDRF));
	wdt_disable();
}

/* this is called once TCNT2 = OCR2A = 249 *
 * resulting in a 1 ms cycle time, 1kHz    */
volatile bool current_state = false;
ISR (TIMER2_COMPA_vect)
{
	current_state = !current_state;
}

int main()
{
	using namespace supreme;

	/* jump to bootloader on reset-pin pulled low */
	if (bit_is_set(MCUSR, EXTRF)) {
		MCUSR &= ~(_BV(EXTRF));
		start_bootloader();
	}

	/* check for stable supply voltage before starting */
	supreme::adc::enable();
	uint8_t i = 0;
	while(supreme::read_supply_voltage_mV() < constants::stable_voltage_threshold_mV)
	{
		board.red_toggle(); _delay_ms(64); // ~0.5s @ 2MHz
		if (++i > 10)
			energymodule_core::suspend(true);
	}

	/* clock is set to be div8 (2MHz) by default, to ensure safe power up.
	   however, when safe voltage is reached, set clock to 16MHz */
	cli();
	CLKPR = (1 << CLKPCE); // Enable change of CLKPS bits
	CLKPR = 0; // Set prescaler to 1
	sei();

	/* show us your reset reason:
	   Bit 3 - WDRF Watchdog System Reset
	   Bit 2 - BORF Brown-out Reset
	   Bit 1 - EXTRF External Reset
	   Bit 0 - PORF Power-on Reset
	*/
	/*
	if (MCUSR != 0) {
		blink(MCUSR);
		MCUSR = 0; // clear reset flag register
	}
	*/

	/* Do NOT allow for battery charging during initialization!
	   We need to charge a large supercap later in initialization
	   and this would likely overload the charger chip's capabilities.
	   Also this is a "battery connected"-check for free: If no battery
	   is connected this would lead to a gentle reset loop of 3sec length.
	*/
	board.battery_charging_off();
	board.red_on();
	_delay_ms(2000);
	board.red_off();



	/* Do not continue operation with wrong fuses set. */
	if (not check_fuses(constants::LF, constants::HF, constants::EF))
		stop(constants::assertion::wrong_fuse_bits_set);

	supreme::adc::init();
	supreme::adc::restart();

	energymodule_core core;


	/* Design of the 1kHz main loop:
	 * 16Mhz clock, prescaler 64 -> 16.000.000 / 64 = 250.000 increments per second
	 * diveded by 1000 -> 250 increments per ms
	 * hence, timer compare register to 250-1 -> ISR inc ms counter -> 1kHz loop
	 *
	 * configure timer 2:
	 */
	TCCR2A = (1<<WGM21);             // CTC mode
	TCCR2B = (1<<CS22);              // set prescaler to 64
	OCR2A = 249;                     // set timer compare register to 250-1
	TIMSK2 = (1<<OCIE2A);            // enable compare interrupt

	communication_ctrl<energymodule_core> com(core);
	bool previous_state = false;

	sei(); // enable interrupts

	while(1) /* main loop */
	{
		if (com.step()) {
			core.halt();
			start_bootloader();
		}
		if (current_state != previous_state) {
			//board.red_on();     // red led on, begin of cycle
			core.step();
			supreme::adc::restart();
			//board.red_off();    // red led off, end of cycle
			previous_state = current_state;
		}
	}
	return 0;
}
