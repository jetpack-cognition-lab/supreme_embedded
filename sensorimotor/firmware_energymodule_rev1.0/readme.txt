
In case your system does not support for building code for the ATmega328PB microcontroller,
download ATmega's support pack from Microchip's website http://packs.download.atmel.com/

... and extract the relevant files (replace x.y.zzz with the current version number):

unzip -j Atmel.ATmega_DFP.x.y.zzz.atpack \
 gcc/dev/atmega328pb/avr5/crtatmega328pb.o \
 gcc/dev/atmega328pb/avr5/libatmega328pb.a \
 include/avr/iom328pb.h


Copy the .o and .a files to /usr/lib/avr/lib/

    sudo cp *.o *.a /usr/lib/avr/lib/



Copy the .h file to /usr/lib/avr/include/avr/

    sudo cp *.h /usr/lib/avr/include/avr/



And add the following snipped to /usr/lib/avr/include/avr/io.h

The folders might vary from system to system, so you should check for similar files and folder names and not create new folders.

  #elif defined (__AVR_ATmega328PB__)
  #  include <avr/iom328pb.h>

should compile now.

with compile flag -mmcpu=atmega328pb


## AVRDUDE

avrdude

Add the following to /etc/avrdude.conf, right below the definition for ATmega328P.

part parent "m328"
	id          = "m328pb";
	desc        = "ATmega328PB";
	signature   = 0x1e 0x95 0x16;

	ocdrev      = 1;
;

Happy flashing!

